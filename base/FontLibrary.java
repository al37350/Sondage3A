package base;

import java.awt.*;

public class FontLibrary{
	public static Font fontTitre = new Font("Liberation Sans", Font.BOLD, 38);
	public static Font fontLabel = new Font("Calibri",0,18);
	public static Font fontTextField = new Font("Calibri",0,25);
	public static Font fontBouton = new Font("Calibri",Font.BOLD,22);
	public static Font fontErreur = new Font("Calibri", Font.BOLD, 28);
	public static Font connexion1 = new Font("Calibri", Font.BOLD, 18);
	public static Font connexion2 = new Font("Calibri", 0, 16);
	public static Font policeGrandTitre = new Font("Liberation Sans",0, 75);

	public static Font utilisateur = new Font("Calibri", 0, 25);



	
	public static String parseString(String init){
		String [] listeMot = init.split(" ");
		int taille = 0;
		String res = "<html>";
		boolean done = false;
		for (String mot: listeMot){
			if (taille < 30 | (taille > 30 && done) | mot.equals("?")){
				res +=  mot + " ";
				taille += mot.length();
			} else {
				res += "<br>" + mot + " ";
				done = true;
			}
		}
		res += "</html>";
		return res;
	}
}