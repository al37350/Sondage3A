package base;

import javax.swing.*;
import java.awt.*;


import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;
import controleur.*;

public class Application extends JFrame{
	public static ImageIcon logo, annuler, deconnection, editer, plus, precedent, suivant, valider, miniAnnuler, miniEditer;
	public static ConnectionMySql connection;
	public static Utilisateur user;
	public JPanel vue;

	public Application(){
		super("Rapid'Sond V1.042");
		this.setSize(1200, 860);
		this.setMinimumSize(new Dimension(1000, 770));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setIconImage(this.logo("connexion.png", 100, 100));
		Container fenetre = this.getContentPane();
		
		connection = new ConnectionMySql();
		
		loadImgs();

		
		this.loadVue("connexion");
		this.setVisible(true);
	}

	public void loadVue(String nomVue){
		try{
			this.remove(vue);
		} catch (Exception e){}

		try{
			switch (nomVue){
				case "connexion": vue = new Vue_connection(this); this.add((Vue_connection)vue); this.user = null; break;

				// Vues d'accueil
				case "concepteur": vue = new Vue_concepteur_accueil(this); this.add((Vue_concepteur_accueil)vue); break;
				case "sondeur": vue = new Vue_sondeur_accueil(this); this.add((Vue_sondeur_accueil)vue); break;
				case "analyste": vue = new Vue_analyste_accueil(this); this.add((Vue_analyste_accueil)vue); break;

				// Sous vues de Concepteur
				case "concepteur_ajout_client" : vue = new Vue_concepteur_CreaClient(this); this.add((Vue_concepteur_CreaClient)vue); break;
				

				case "concepteur_creation_sondage" : 
					if (Vue_concepteur_accueil.vcs == null){
						vue = new Vue_concepteur_creation_sondage(this); this.add((Vue_concepteur_creation_sondage)vue); break;
					} else {
						vue = Vue_concepteur_accueil.vcs;
					}
				case "concepteur_edition_client" : vue = new Vue_concepteur_modifierClient(this); this.add((Vue_concepteur_modifierClient)vue); break;
				case "concepteur_modif_sondage" : vue = new Vue_concepteur_edition_questionnaire_liste(this); this.add((Vue_concepteur_edition_questionnaire_liste)vue); break;
				case "concepteur_edit_client" : vue = new Vue_concepteur_modif_client(this,Vue_concepteur_modifierClient.cli);this.add((Vue_concepteur_modif_client)vue);break;
				case "concepteur_modif_sondage2" : vue = new Vue_concepteur_modif_questionnaire(this,Vue_concepteur_edition_questionnaire_liste.q); this.add((Vue_concepteur_modif_questionnaire)vue); break;
				case "concepteur_creation_question" : vue = new Vue_concepteur_crea_question(this); this.add((Vue_concepteur_crea_question)vue); break;
				case "concepteur_ajout_client_sondage":vue= new Vue_concepteur_ajout_client_sondage(this);this.add((Vue_concepteur_ajout_client_sondage)vue); break;

				// Sous vues de Sondeur
				case "questionnaire": vue = new Vue_sondeur_sondage(this); this.add((Vue_sondeur_sondage)vue); break;

				// Sous vues de Analyste
				case "analyste_question" : vue = new Vue_analyste_question(this); this.add((Vue_analyste_question)vue); break;
			}
		} catch (NullPointerException e2){
			System.out.println(e2.getCause());
		}
		this.refresh();

	}
	

	public void refresh(){
		this.revalidate();
		this.repaint();
	}


	public void loadImgs(){
		logo = resize("logo.png", 400, 210);
		annuler = resize("annulerLegende.png", 100, 100);
		deconnection = resize("deconnection.png", 100, 100);
		editer = resize("editer.png", 100, 100);
		plus = resize("plus.png", 100, 100);
		precedent = resize("precedent.png", 100, 100);
		suivant = resize("suivant.png", 100, 100);
		valider = resize("valider.png", 100, 100);

		// mini-icônes
		miniAnnuler = resize("annuler.png", 20, 20);
		miniEditer = resize("editer.png", 20, 20);
	}

	public ImageIcon resize(String nom, int hauteur, int largeur){
		ImageIcon original = new ImageIcon("./img/" + nom);
		if (original.getImageLoadStatus () == MediaTracker.COMPLETE ){
			Image temp = original.getImage().getScaledInstance(hauteur, largeur, Image.SCALE_SMOOTH); 
			return new ImageIcon(temp); 
		} else {
			return null;
		}
	}

	public Image logo(String nom, int hauteur, int largeur){
		ImageIcon original = new ImageIcon("./img/" + nom);
		if (original.getImageLoadStatus () == MediaTracker.COMPLETE ){
			Image temp = original.getImage().getScaledInstance(hauteur, largeur, Image.SCALE_SMOOTH); 
			return temp; 
		} else {
			return null;
		}
	}
}