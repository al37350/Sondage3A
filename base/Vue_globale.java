package base;

import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public abstract class Vue_globale extends JPanel{
	public JPanel barrePrinc;
	public JPanel droite;
	public JPanel gauche, gaucheBis;
	public JPanel milieu;
	public Application fenetre;
	
	public Vue_globale(JFrame fenetre, String location){
		this(fenetre, "basic", "basic", "basic", location);		
	}

	public Vue_globale(JFrame fenetre, String typeG, String typeM, String typeD, String location){
		super();		
		this.fenetre = (Application)fenetre;
		this.setLayout(new BorderLayout());
		
		barrePrinc = new JPanel();
		barrePrinc.setLayout(new BorderLayout());
		barrePrinc.setBorder(BorderFactory.createLineBorder(Color.black));
		barrePrinc.setPreferredSize(new Dimension(0,125));
		
		this.setBackground(ColorLibrary.beige);


		// basic : partie gauche de la barre d'info -> Boutton de déconnexion seul.
		// doubleB : partie gauche de la barre d'info -> Boutton d'annulation et boutton de validation.
		// annuler : partie gauche de la barre d'info -> Boutton d'annulation seul.
		// retour :  partie gauche de la barre d'info -> Boutton retour seul.
		switch (typeG){
			case "basic":
				gauche = new JPanel();
				gauche.setLayout(new GridLayout(1,1));
				gauche.setBorder(BorderFactory.createLineBorder(Color.black));
				gauche.setBackground(ColorLibrary.bleu);
				gauche.setPreferredSize(new Dimension(125,0));
				gauche.add(new BoutonDeconnexion(fenetre, location));

				barrePrinc.add(gauche, "West");
				break;


			case "doubleB":
				gauche = new JPanel();
				gauche.setLayout(new GridLayout(1,1));
				gauche.setBorder(BorderFactory.createLineBorder(Color.black));
				gauche.setBackground(ColorLibrary.bleu);
				gauche.setPreferredSize(new Dimension(125,0));
				gauche.add(new BoutonAnnuler(fenetre, location));

				gaucheBis = new JPanel();
				gaucheBis.setLayout(new GridLayout(1,1));
				gaucheBis.setBorder(BorderFactory.createLineBorder(Color.black));
				gaucheBis.setBackground(ColorLibrary.bleu);
				gaucheBis.setPreferredSize(new Dimension(125,0));
				gaucheBis.add(new BoutonValider(fenetre, location));

				JPanel gridGauche = new JPanel();
				gridGauche.setLayout(new GridLayout(1, 2));
				gridGauche.add(gauche);
				gridGauche.add(gaucheBis);
				barrePrinc.add(gridGauche, "West");
				break;


			case "annuler":
				gauche = new JPanel();
				gauche.setLayout(new GridLayout(1,1));
				gauche.setBorder(BorderFactory.createLineBorder(Color.black));
				gauche.setBackground(ColorLibrary.bleu);
				gauche.setPreferredSize(new Dimension(125,0));
				gauche.add(new BoutonAnnuler(fenetre, location));

				barrePrinc.add(gauche, "West");
				break;


			case "retour":
				gauche = new JPanel();
				gauche.setLayout(new GridLayout(1,1));
				gauche.setBorder(BorderFactory.createLineBorder(Color.black));
				gauche.setBackground(ColorLibrary.bleu);
				gauche.setPreferredSize(new Dimension(125,0));
				gauche.add(new BoutonAnnuler(fenetre, location));

				barrePrinc.add(gauche, "West");
				break;
		}

		FlowLayout layoutMilieu = new FlowLayout();

		// basic : partie gauche de la barre d'info -> Texte Rapid Sond.
		// Empty : partie gauche de la barre d'info -> JPanel vide pour mettre ce que l'on veut.
		switch (typeM){
			
			case "basic":
				milieu = new JPanel();
				
				layoutMilieu.setAlignment(FlowLayout.CENTER);
				layoutMilieu.setVgap(15);
				milieu.setLayout(layoutMilieu);
				milieu.setBorder(BorderFactory.createLineBorder(Color.black));		
				milieu.setBackground(ColorLibrary.bleu);

				JLabel rapid = new JLabel("Rapid'Sond");		

				rapid.setFont(FontLibrary.policeGrandTitre);
				milieu.add(rapid);
				
				barrePrinc.add(milieu, "Center");
				break;

			case "empty":
				milieu = new JPanel();
				
				layoutMilieu.setAlignment(FlowLayout.CENTER);
				layoutMilieu.setVgap(15);
				milieu.setLayout(layoutMilieu);
				milieu.setBorder(BorderFactory.createLineBorder(Color.black));		
				milieu.setBackground(ColorLibrary.bleu);

				barrePrinc.add(milieu, "Center");
				break;
		}

		// basic : partie gauche de la barre d'info -> Information de l'utilisateur connecté.
		// progressBar : partie gauche de la barre d'info -> Progress bar.
		switch (typeD){
			case "basic":
				droite = new JPanel();
				droite.setLayout(new FlowLayout());
				droite.setBorder(BorderFactory.createLineBorder(Color.black));
				droite.setBackground(ColorLibrary.bleu);
				droite.setPreferredSize(new Dimension(350,0));

				GridLayout layoutDroite = new GridLayout(2, 1);
				layoutDroite.setVgap(-30);
				droite.setLayout(layoutDroite);

				JLabel np = new JLabel("     " + Application.user.getPrenom() + " " + Application.user.getNom());
				np.setFont(FontLibrary.utilisateur);
				JLabel role = new JLabel("     " + Application.user.getRole());
				role.setFont(FontLibrary.utilisateur);

				droite.add(np);
				droite.add(role);

				barrePrinc.add(droite, "East");
				break;

			case "progressBar":
				droite = new JPanel();
				droite.setLayout(new FlowLayout());
				droite.setBorder(BorderFactory.createLineBorder(Color.black));
				droite.setBackground(ColorLibrary.bleu);
				droite.setPreferredSize(new Dimension(350,0));

				barrePrinc.add(droite, "East");
				break;
		}

		this.add(barrePrinc,BorderLayout.NORTH);
	}
}