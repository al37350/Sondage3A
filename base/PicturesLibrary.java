package base;

import javax.swing.*;
import java.awt.*;

public class PicturesLibrary{
	// icônes normaux
	public static ImageIcon	logo = resize("logo.png", 400, 210);
	public static ImageIcon	annuler = resize("annulerLegende.png", 100, 100);
	public static ImageIcon	deconnexion = resize("deconnexion.png", 100, 100);
	public static ImageIcon	editer = resize("editer.png", 100, 100);
	public static ImageIcon	plus = resize("plus.png", 200, 100);
	public static ImageIcon	precedent = resize("precedent.png", 125, 125);
	public static ImageIcon	suivant = resize("suivant.png", 125, 125);
	public static ImageIcon	valider = resize("valider.png", 100, 100);
	public static ImageIcon	retour = resize("retour.png", 100, 100);

	// mini-icônes
	public static ImageIcon	miniAnnuler = resize("annuler.png", 25, 25);
	public static ImageIcon	miniEditer = resize("editer.png", 30, 30);


	public static ImageIcon resize(String nom, int hauteur, int largeur){
		ImageIcon original = new ImageIcon("./img/" + nom);
		if (original.getImageLoadStatus () == MediaTracker.COMPLETE){
			Image temp = original.getImage().getScaledInstance(hauteur, largeur, Image.SCALE_SMOOTH); 
			return new ImageIcon(temp); 
		} else {
			return null;
		}
	}
}