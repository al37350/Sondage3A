package base;

import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public class Vue_connection extends JPanel{
	public Vue_connection(JFrame fenetre){
		super();
		this.setLayout(new GridLayout(1, 2));
		this.setBackground(ColorLibrary.bleu);

		
		JPanel centreForm = new JPanel();
		centreForm.setLayout(new GridLayout(3, 1));
		centreForm.setBackground(new Color(0, 0, 0, 0));
		centreForm.setAlignmentY(Component.CENTER_ALIGNMENT);
		
		JPanel form = new JPanel();
		form.setLayout(new GridLayout(6, 1));
		form.setBackground(new Color(0, 0, 0, 0));

		JPanel resizedForm = new JPanel();
		resizedForm.setBackground(new Color(0, 0, 0, 0));

		JPanel centreLogo = new JPanel();
		centreLogo.setLayout(new BorderLayout());
		centreLogo.setBackground(new Color(0, 0, 0, 0));

		JPanel empty1 = new JPanel();
		empty1.setBackground(new Color(0, 0, 0, 0));
		JPanel empty2 = new JPanel();
		empty2.setBackground(new Color(0, 0, 0, 0));


		JTextField id = new JTextField();
		id.setPreferredSize(new Dimension(300, 40));
		id.setFont(FontLibrary.connexion2);
		JPasswordField mdp = new JPasswordField();
		mdp.setPreferredSize(new Dimension(300, 40));
		mdp.setFont(FontLibrary.connexion2);
		
		JLabel labelId = new JLabel("Identifiant :");
		labelId.setFont(FontLibrary.connexion1);

		JLabel labelMdp = new JLabel("Mot de passe :");
		labelMdp.setFont(FontLibrary.connexion1);

		JLabel labelStatus = new JLabel("");

		JButton connection = new JButton("Connexion");
		connection.setBackground(Color.WHITE);

		connection.addActionListener(new Connection(fenetre, id, mdp, labelStatus));
		id.addActionListener(new Connection(fenetre, id, mdp, labelStatus));
		mdp.addActionListener(new Connection(fenetre, id, mdp, labelStatus));

		



		form.add(labelId);
		form.add(id);
		form.add(labelMdp);
		form.add(mdp);
		form.add(labelStatus);
		form.add(connection);


		resizedForm.add(form);

		centreForm.add(Box.createVerticalGlue());
		centreForm.add(resizedForm);
		centreForm.add(Box.createVerticalGlue());

		centreLogo.add(new JLabel(Application.logo), "Center");

		this.add(centreLogo);
		this.add(centreForm);
	}
}