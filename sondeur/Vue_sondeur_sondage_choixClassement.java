package sondeur;

import Modele.*;
import base.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

import java.util.ArrayList;

public class Vue_sondeur_sondage_choixClassement extends Vue_sondeur_sondage_choixGeneral{
	private JCheckBox [] listeBoutons;
	
	public Vue_sondeur_sondage_choixClassement(int numQ){
		super(numQ);
		
		JPanel panelReponse = new JPanel();

		int max = question.getPropositions().size();

		listeBoutons = new JCheckBox [max];

		for (int i = 0; i < max; i++){
			JCheckBox r = new JCheckBox(question.getProposition(i).getTexte());
			r.setName(Integer.toString(question.getProposition(i).getIdValPossible()));
			r.setPreferredSize(new Dimension(100,100));
			r.setFont(FontLibrary.fontLabel);
			panelReponse.add(r);
			listeBoutons[i] = r;
		}

		this.add(panelReponse, "Center");
	}

	public ArrayList<JCheckBox> getReponse(){
		ArrayList<JCheckBox> result = new ArrayList<JCheckBox>();
		for (JCheckBox checkBox: listeBoutons){
			if (checkBox.isSelected()){
				result.add(checkBox);
			}
		}
		return result;
	}
}
