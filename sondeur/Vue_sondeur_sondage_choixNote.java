package sondeur;

import Modele.*;
import base.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public class Vue_sondeur_sondage_choixNote extends Vue_sondeur_sondage_choixGeneral{
	public int nbClicks;
	private ButtonGroup bg;
	private JRadioButton [] listeBoutons;

	public Vue_sondeur_sondage_choixNote(int numQ){
		super(numQ);

		nbClicks = 0;
		
		bg = new ButtonGroup();
		JPanel panelReponse = new JPanel();

		int max = 11;

		listeBoutons = new JRadioButton [max];


		for (int i = 0; i < max; i++){
			JRadioButton r = new JRadioButton(Integer.toString(i));
			r.setPreferredSize(new Dimension(100,100));
			r.setFont(FontLibrary.fontLabel);			
			bg.add(r);
			listeBoutons[i] = r;
			panelReponse.add(r);
		}
		
		this.add(panelReponse, "Center");
	}

	public String getReponse(){
		for (JRadioButton j: listeBoutons){
			if (j.isSelected()){
				return j.getText();
			}
		}
		return "";
	}
}
