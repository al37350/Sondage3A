package sondeur;

import Modele.*;
import base.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public class Vue_sondeur_sondage_choixLibre extends Vue_sondeur_sondage_choixGeneral{
	private JTextField fieldReponse;
	public Vue_sondeur_sondage_choixLibre(int numQ){
		super(numQ);

		JPanel panelReponse = new JPanel();
			
		fieldReponse = new JTextField();
		fieldReponse.setPreferredSize(new Dimension(400, 50));

		panelReponse.add(fieldReponse);
		this.add(panelReponse, "Center");
	}

	public String getReponse(){
		return fieldReponse.getText();
	}
}