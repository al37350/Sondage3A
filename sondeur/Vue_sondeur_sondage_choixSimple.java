package sondeur;

import Modele.*;
import base.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public class Vue_sondeur_sondage_choixSimple extends Vue_sondeur_sondage_choixGeneral{
	private ButtonGroup bg;
	private JRadioButton [] listeBoutons;

	public Vue_sondeur_sondage_choixSimple(int numQ){
		super(numQ);

		JPanel panelReponse = new JPanel();

		
		bg = new ButtonGroup();
		int max = question.getPropositions().size();

		listeBoutons = new JRadioButton [max];
		for (int i = 0; i < max; i++){
			JRadioButton r = new JRadioButton(question.getProposition(i).getTexte());
			r.setPreferredSize(new Dimension(100,100));
			r.setFont(FontLibrary.fontLabel);
			
			listeBoutons[i] = r;
			panelReponse.add(r);

			bg.add(r);
		}

		this.add(panelReponse, "Center");
	}

	public String getReponse(){
		for (JRadioButton j: listeBoutons){
			if (j.isSelected()){
				return j.getText();
			}
		}
		return "";
	}
}