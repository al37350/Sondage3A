package sondeur;

import Modele.*;
import base.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public abstract class Vue_sondeur_sondage_choixGeneral extends JPanel{
	protected Question question;
	protected String reponse;

	public Vue_sondeur_sondage_choixGeneral(int numQ){
		super();
		
		reponse = "";
		
		question = Vue_sondeur_sondage.q.getQuestion(numQ);

		Font font1 = new Font("Calibri", 0, 40);
		

		JLabel labelQuestion = new JLabel(FontLibrary.parseString(question.getEnonce()));
		labelQuestion.setHorizontalAlignment(JLabel.CENTER);
		labelQuestion.setFont(font1);

		FlowLayout layout = new FlowLayout();
		layout.setVgap(50);

		JPanel panelLabelQuestion = new JPanel(layout);

		panelLabelQuestion.add(labelQuestion);
		
		BorderLayout layoutVue = new BorderLayout();
		layoutVue.setVgap(120);
		this.setLayout(layoutVue);
		
		this.add(panelLabelQuestion, "North");
	}

	public Question getQuestion(){
		return this.question;
	}
}
