package sondeur;

import base.*;
import Modele.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public class Vue_sondeur_accueil extends Vue_globale{
	private JLabel numero, identite, questionnaire;

	public static int sondage;
	public static Sonde sonde;

	public Vue_sondeur_accueil(JFrame fenetre){
		super(fenetre, "sondeur");

		Font font1 = new Font("Calibri", Font.BOLD, 40);
		Font font2 = new Font("Calibri", 0, 40);
		Font font3 = new Font("Calibri", 0, 20);
		

		JPanel center = new JPanel();
		center.setLayout(new FlowLayout());
		center.setPreferredSize(new Dimension(1000, 800));

		JPanel main = new JPanel();
		main.setLayout(new GridLayout(5, 1));	
		

		JLabel legende = new JLabel("Personne à interroger :");
		legende.setHorizontalAlignment(JLabel.CENTER);
		legende.setFont(font1);

		numero = new JLabel("");
		numero.setHorizontalAlignment(JLabel.CENTER);
		numero.setFont(font2);

		identite = new JLabel("");
		identite.setHorizontalAlignment(JLabel.CENTER);
		identite.setFont(font2);

		questionnaire = new JLabel("");
		questionnaire.setHorizontalAlignment(JLabel.CENTER);
		questionnaire.setFont(font3);

		main.add(legende);
		main.add(numero);
		main.add(identite);
		main.add(questionnaire);

		JPanel boutons = new JPanel();
		GridLayout layoutBoutons = new GridLayout(1, 2);
		layoutBoutons.setHgap(80);
		boutons.setLayout(layoutBoutons);
		

		JButton ndecroche = new JButton("N'a pas décroché(e)");
		ndecroche.setBackground(new Color(210, 59, 59, 255));
		ndecroche.setPreferredSize(new Dimension(300, 100));
		ndecroche.setFont(font3);
		ndecroche.setFocusPainted(false);
		ndecroche.addActionListener(new ActionBouton(fenetre, "ndecroche"));

		JButton decroche = new JButton("A décroché(e)");
		decroche.setBackground(new Color(158, 244, 109, 255));
		decroche.setPreferredSize(new Dimension(300, 100));
		decroche.setFont(font3);
		decroche.setFocusPainted(false);
		decroche.addActionListener(new ActionBouton(fenetre, "decroche"));

		boutons.add(ndecroche);
		boutons.add(decroche);

		main.add(boutons);

		center.add(main, "Center");
		this.add(center, "Center");

		this.loadNew();
	}

	public void loadNew(){
		try{
			sonde = RqtSondeur.getprochainSonde(Application.connection);
			this.sondage = Integer.parseInt(sonde.getIdQ());

			numero.setText(sonde.getTelephone());
			identite.setText(sonde.getNom() + " " + sonde.getPrenom());
			questionnaire.setText("Questionnaire : " + sonde.getTitreQ());
			this.fenetre.refresh();
		} catch (NullPointerException e){
			numero.setText("Il n'y a personne à interroger.");
			this.fenetre.refresh();
		}
	}
}