package sondeur;

import Modele.*;
import base.*;
import controleur.*;

import java.util.ArrayList;
import javax.swing.*;
import java.awt.*;
import java.awt.Color;

public class Vue_sondeur_sondage extends Vue_globale{
	private int progression, max;
	private JProgressBar progressBar;
	private JLabel legendeProgression;
	private JPanel center;
	private Vue_sondeur_sondage_choixGeneral panelQuestion;
	public static Questionnaire q;
	public static Vue_sondeur_sondage_choixGeneral [] listePanelsReponses;
	public static Reponse [] listeReponses;


	public Vue_sondeur_sondage(JFrame fenetre){
		super(fenetre, "annuler", "empty", "progressBar", "sondeur");

		Font font1 = new Font("Calibri", 0, 30);
 		
 		q = RqtCommunes.getQuestionnaire(Vue_sondeur_accueil.sondage, Application.connection);
 		

		Sonde sonde = Vue_sondeur_accueil.sonde;
		
		JLabel infoSonde = new JLabel(sonde.getNom() + " " + sonde.getPrenom() + " - " + sonde.getTelephone());
		infoSonde.setBackground(new Color(255, 0, 0, 255));
		infoSonde.setVerticalAlignment(JLabel.CENTER);
		infoSonde.setFont(font1);
		milieu.add(infoSonde);

		FlowLayout layoutDroite = new FlowLayout();
		layoutDroite.setAlignment(FlowLayout.CENTER);
		layoutDroite.setVgap(20);
		droite.setLayout(layoutDroite);

		max = q.getNbQuestions();


		listePanelsReponses = new Vue_sondeur_sondage_choixGeneral[max];
		listeReponses = new Reponse [max];

		progression = 1;
		progressBar = new JProgressBar(0, max);
		progressBar.setPreferredSize(new Dimension(300, 30));
		progressBar.setValue(1);
		progressBar.setStringPainted(false);

		legendeProgression = new JLabel("Question : " + progression + "/"  + max);
		droite.add(legendeProgression);
		droite.add(progressBar);

		center = new JPanel();
		center.setLayout(new BorderLayout());
		center.setPreferredSize(new Dimension(1000, 800));


		BoutonPrecedent precedent = new BoutonPrecedent(fenetre, "sondeur");
		precedent.setBackground(new Color(250, 235, 225, 0));
		precedent.setPreferredSize(new Dimension(125, 0));
		precedent.setContentAreaFilled(false);
		precedent.setFocusPainted(false);

		BoutonSuivant suivant = new BoutonSuivant(fenetre, "sondeur");
		suivant.setBackground(new Color(250, 235, 225, 0));
		suivant.setPreferredSize(new Dimension(125, 0));
		suivant.setContentAreaFilled(false);
		suivant.setFocusPainted(false);

		center.add(precedent, "West");
		center.add(suivant, "East");

		center.add(new JPanel(), "Center");
		this.add(center, "Center");
		this.loadQuestion();
	}

	public void loadNext(){
		this.saveAnswer();
		if (progression < max){
			progression += 1;
			legendeProgression.setText("Question : " + progression + "/"  + max);
			progressBar.setValue(progressBar.getValue() + 1);
			this.loadQuestion();
		} else if (progression == max){
			this.validate();
		}
	}

	public void loadPrevious(){
		this.saveAnswer();
		if (progressBar.getValue() > 1){
			progression -= 1;
			legendeProgression.setText("Question : " + progression + "/"  + max);
			progressBar.setValue(progressBar.getValue() - 1);
		}
		this.loadQuestion();
	}


	public void saveAnswer(){
		listePanelsReponses[progression-1] = this.panelQuestion;
	}


	public void loadQuestion(){
		try{
			this.center.remove(panelQuestion);
		} catch (Exception e){}

		if (listePanelsReponses[progression-1] != null){
			panelQuestion = listePanelsReponses[progression-1];
		} else {	
			switch (q.getQuestion(progression-1).getIdTypeQuestion()){
				case ('c'): panelQuestion = new Vue_sondeur_sondage_choixClassement(progression-1); break; 
				case ('l'): panelQuestion = new Vue_sondeur_sondage_choixLibre(progression-1); break;
				case ('m'): panelQuestion = new Vue_sondeur_sondage_choixMultiples(progression-1); break;
				case ('n'): panelQuestion = new Vue_sondeur_sondage_choixNote(progression-1); break;
				case ('u'): panelQuestion = new Vue_sondeur_sondage_choixSimple(progression-1); break;
			}
		}
		this.center.add(panelQuestion, "Center");
		this.fenetre.refresh();
	}


	public void validate(){
		String missing = "";
		boolean ready = true;
		for (int i = 0; i < listePanelsReponses.length; i++){
			char type = listePanelsReponses[i].question.getIdTypeQuestion();

			String rep = "";
			ArrayList<JCheckBox> repListeBoutons = new ArrayList<JCheckBox>();

			switch (type){
				case ('c'): repListeBoutons = ((Vue_sondeur_sondage_choixClassement)listePanelsReponses[i]).getReponse(); break; 
				case ('l'): rep = ((Vue_sondeur_sondage_choixLibre)listePanelsReponses[i]).getReponse(); break; 
				case ('m'): repListeBoutons = ((Vue_sondeur_sondage_choixMultiples)listePanelsReponses[i]).getReponse(); break; 
				case ('n'): rep = ((Vue_sondeur_sondage_choixNote)listePanelsReponses[i]).getReponse(); break; 
				case ('u'): rep = ((Vue_sondeur_sondage_choixSimple)listePanelsReponses[i]).getReponse(); break; 
			}

			if (type == 'c' || type == 'm'){
				if (repListeBoutons.isEmpty()){
					missing += (i+1) + ", ";
					ready = false;
				} else {
					listeReponses[i] = new Reponse(type, i+1, repListeBoutons);
				}
			} else {
				if (rep.equals("")){
					missing += (i+1) + ", ";
					ready = false;
				} else {
					listeReponses[i] = new Reponse(type, i+1, rep);
				}
			}
		}

		if (ready){
			Object[] options = {"Oui", "Non"};
		    int n = JOptionPane.showOptionDialog(null, "Le questionnaire est il prêt à être envoyé ?", "Envoie du questionnaire", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, (Icon)PicturesLibrary.valider, options, options[1]);
		    if (n == 0){
		    	RqtSondeur.setReponses(q, listeReponses, Vue_sondeur_accueil.sonde, Application.connection);
				this.fenetre.loadVue("sondeur");
			}
		} else {
			Object[] options = {"Continuer"};
		    int n = JOptionPane.showOptionDialog(null, "Certaines questions n'ont pas été rempli (" + (missing.substring(0, missing.length()-2)) + ")", "Envoie du questionnaire", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, (Icon)PicturesLibrary.retour, options, options[0]);
			progression = Integer.parseInt(missing.substring(0, 1));

			legendeProgression.setText("Question : " + progression + "/"  + max);
			progressBar.setValue(progression);
			this.loadQuestion();
		}
	}
}