package controleur;

import base.*;

import javax.swing.*;

public class BoutonRetour extends BoutonSimple{
	public BoutonRetour(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.retour, "retour", section, ColorLibrary.beige);
	}
}