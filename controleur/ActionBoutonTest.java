package controleur;

import base.*;
import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;

import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.filechooser.*;
import java.io.*;

public class ActionBoutonTest implements ActionListener{
	String nom;
	Application fenetre;

	public ActionBoutonTest(JFrame fenetre, String nom){
		super();
		this.nom = nom;
		this.fenetre = (Application)fenetre;
	}

	public void actionPerformed(ActionEvent e){
		if (nom.equals("parcourir")){
			// création du JFileChooser
			JFileChooser fc = new JFileChooser("~");
			// cr ́eation d’un filtre
			FileNameExtensionFilter filtrePDF=	new FileNameExtensionFilter("Fichiers PDF","pdf");
			// ajout du filtrePDF
			fc.addChoosableFileFilter(filtrePDF);
			// mettre ce filtrePDF comme filtrePDF par defaut
			fc.setFileFilter(filtrePDF);
			// mettre un fichier par défaut
			fc.setSelectedFile(new File("Le_nom_du_pdf.pdf"));
			int res=fc.showSaveDialog(null);
			//appuyer sur enregistrer
			if(res==0){
				((Vue_analyste_resultat_sondage)this.fenetre.vue).nomRep.setText(fc.getCurrentDirectory().getAbsolutePath());
			}
		}
		else if(nom.equals("generer")){
			//on va chercher le chemin et le nom du fichier et on me tout ca dans un String
			//on met try si jamais il y a une exception
			try
			{
				/**
				 * BufferedWriter a besoin d un FileWriter, 
				 * les 2 vont ensemble, on donne comme argument le nom du fichier
				 * true signifie qu on ajoute dans le fichier (append), on ne marque pas par dessus 
				 */
				FileWriter fw = new FileWriter(((Vue_analyste_resultat_sondage)this.fenetre.vue).nomRep.getText()+"/mytextfile.html", true);
				
				// le BufferedWriter output auquel on donne comme argument le FileWriter fw cree juste au dessus
				BufferedWriter output = new BufferedWriter(fw);
				
				//on marque dans le fichier ou plutot dans le BufferedWriter qui sert comme un tampon(stream)
				output.write("test");
				//on peut utiliser plusieurs fois methode write
				
				output.flush();
				//ensuite flush envoie dans le fichier, ne pas oublier cette methode pour le BufferedWriter
				
				output.close();
				//et on le ferme
				this.displayValidGenPDFDialog(e);
				this.fenetre.loadVue("analyste");
			}
			catch(IOException ioe){
				System.out.print("Erreur : ");
				ioe.printStackTrace();
				}
		}

	}

	public void displayValidGenPDFDialog(ActionEvent e){
		Object[] options = {"Ok"};
	    JOptionPane.showOptionDialog((JButton)e.getSource(), "Le pdf à bien été enregistré", "PDF enregistré", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, (Icon)Application.valider, options, options[0]);
	}
}