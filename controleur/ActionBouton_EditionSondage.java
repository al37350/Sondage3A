package controleur;

import base.*;
import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;

import java.awt.event.*;
import java.awt.Component;
import java.awt.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.Color;
import Modele.*;
import java.util.ArrayList;

public class ActionBouton_EditionSondage extends ActionBouton{
	String nom;
	Application fenetre;
	Vue_concepteur_edition_questionnaire_liste vue;
	

	public ActionBouton_EditionSondage(JFrame fenetre, String nom, Vue_concepteur_edition_questionnaire_liste vue){
		super(fenetre,nom);
		this.nom = nom;
		this.fenetre=(Application)fenetre;
		this.vue=vue;
	}
	@Override
	public void actionPerformed(ActionEvent e){
		switch (this.nom){
			// case "éditer_question" : 
			// case "éditer_client" :
			// case "éditer_panel" :
			case "modif_supprimer":
				int idQ = Vue_concepteur_edition_questionnaire_liste.listeSond.get(Integer.parseInt(((Component)e.getSource()).getParent().getName())-1).getidQ();
				Object[] options = {"Oui", "Non"}; 
				int n = JOptionPane.showOptionDialog(null, "Etes vous sur de vouloir supprimer ce sondage ?", "Suppression", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, (Icon)PicturesLibrary.miniAnnuler, options, options[1]);
				if (n==0){
				    RqtConcepteur.deleteQuestionnaire(idQ, Application.connection);
					Vue_concepteur_edition_questionnaire_liste.listeSond = RqtAnalyste.getAllQuestionnaire(Application.connection);
					this.fenetre.loadVue("concepteur_modif_sondage"); 
				}
				break;

			case "modif_sondage" : 
				int index = Integer.parseInt(((Component)e.getSource()).getParent().getName());
				Vue_concepteur_edition_questionnaire_liste.q = vue.listeSond.get(index-1);
				this.fenetre.loadVue("concepteur_modif_sondage2"); 
				break;
		}
	}
}