package controleur;

import base.*;
import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;

import java.awt.event.*;
import java.awt.Component;
import java.awt.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.Color;
import Modele.*;
import java.util.ArrayList;

public class ActionBouton_concepteur extends ActionBouton{
	String nom;
	Application fenetre;
	

	public ActionBouton_concepteur(JFrame fenetre, String nom){
		super(fenetre,nom);
		this.nom = nom;
		this.fenetre=(Application)fenetre;
	}
	@Override
	public void actionPerformed(ActionEvent e){
		switch (this.nom){
			// case "éditer_question" : 
			// case "éditer_client" :
			// case "éditer_panel" :
			case "precedent_question" : this.retourQuestion(e); break;
			case "suivant_question" : this.enregistreQuestion(e); break; 
			case "ajouter_reponse" : this.displayDialogReponse(e); break;
			case "precedent_question_analyse" : ((Vue_analyste_question)this.fenetre.vue).loadPrevious(); break;
			case "suivant_question_analyse" : ((Vue_analyste_question)this.fenetre.vue).loadNext(); break;
			// case "supprimer_question" : 
			// case "éditer_question" :
			case "ajout_client" : fenetre.loadVue("concepteur_ajout_client"); break;
			case "creation_sondage" : fenetre.loadVue("concepteur_creation_sondage"); break;
			case "edition_client" : fenetre.loadVue("concepteur_edition_client"); break;
			case "modif_sondage" : fenetre.loadVue("concepteur_modif_sondage"); break;
			case "valider" :
				Vue_concepteur_crea_question vueCreaQ = ((Vue_concepteur_crea_question)fenetre.vue);
				if (!vueCreaQ.intituler.getText().isEmpty()){
					if ((vueCreaQ.listeReponses.getComponentCount()!=0 && vueCreaQ.type!='l') || (vueCreaQ.type == 'l')){

						vueCreaQ.quest.setEnonce(vueCreaQ.intituler.getText());
						vueCreaQ.quest.setIdTypeQuestion(vueCreaQ.type);
						vueCreaQ.qu.addQuestion(vueCreaQ.quest);
					}
				}
				System.out.println(Application.user.getId());
				RqtConcepteur.setQuestionnaire(((Vue_concepteur_crea_question)fenetre.vue).qu, 
												RqtConcepteur.findClient(((Vue_concepteur_crea_question)fenetre.vue).idClient,
												Application.connection), 
												Application.user,
												Application.connection); 
				fenetre.loadVue("concepteur"); 
				break;
			case "suppr_question" : this.actionSuprr(e); break;
		}
	}

	public void displayDialogReponse(ActionEvent e){
			if (!((Vue_concepteur_crea_question)fenetre.vue).intituler.getText().isEmpty()){
				String s = JOptionPane.showInputDialog(this.fenetre,"Rentrez la valeur de la réponse à ajouter ","Rentrez votre réponse",JOptionPane.QUESTION_MESSAGE);
				if (s!=null && s.length()!=0){
				ValPossible val = new ValPossible(((Vue_concepteur_crea_question)fenetre.vue).qu.getidQ(),((Vue_concepteur_crea_question)fenetre.vue).numQuestionActuelle+1,((Vue_concepteur_crea_question)fenetre.vue).quest.getNumNextProposition(),s);
				((Vue_concepteur_crea_question)fenetre.vue).quest.addProposition(val);
				((Vue_concepteur_crea_question)fenetre.vue).listeReponses.add(((Vue_concepteur_crea_question)fenetre.vue).addElem(val,fenetre));
				((Vue_concepteur_crea_question)fenetre.vue).listeReponses.setPreferredSize(new Dimension(550,((Vue_concepteur_crea_question)fenetre.vue).cpt));
				((Vue_concepteur_crea_question)fenetre.vue).quest.setEnonce(((Vue_concepteur_crea_question)fenetre.vue).intituler.getText());
				if(!((Vue_concepteur_crea_question)fenetre.vue).qu.contient(((Vue_concepteur_crea_question)fenetre.vue).quest.getEnonce())){
					((Vue_concepteur_crea_question)fenetre.vue).qu.addQuestion(((Vue_concepteur_crea_question)fenetre.vue).quest);
				}
				else{
					if (!((Vue_concepteur_crea_question)fenetre.vue).quest.contientValPossible(val)){
						((Vue_concepteur_crea_question)fenetre.vue).quest.addProposition(val);
					}
				}
				((Vue_concepteur_crea_question)fenetre.vue).fond2.remove(((Vue_concepteur_crea_question)fenetre.vue).placement_erreur);
			}
			((Vue_concepteur_crea_question)fenetre.vue).fenetre.refresh();
			// System.out.println(((Vue_concepteur_crea_question)fenetre.vue).quest.getPropositions());

		}
	}

	public void enregistreQuestion(ActionEvent e){
		Vue_concepteur_crea_question vueCreaQ = ((Vue_concepteur_crea_question)fenetre.vue);
		if (!vueCreaQ.intituler.getText().isEmpty()){
			if ((vueCreaQ.listeReponses.getComponentCount()!=0 && vueCreaQ.type!='l') || (vueCreaQ.type == 'l')){
				vueCreaQ.quest.setEnonce(vueCreaQ.intituler.getText());
				vueCreaQ.quest.setIdTypeQuestion(vueCreaQ.type);
				if(!vueCreaQ.qu.contient(vueCreaQ.quest.getEnonce())){
					vueCreaQ.qu.addQuestion(vueCreaQ.quest);
				}
			

				vueCreaQ.quest = new Question("", ' ', vueCreaQ.qu.getidQ(), vueCreaQ.qu.getNumNextQuestion());
				vueCreaQ.listeReponses.setPreferredSize(new Dimension(550,vueCreaQ.cpt));
			

				vueCreaQ.intituler.setText("");
				vueCreaQ.listeReponses.repaint();
				vueCreaQ.listeReponses.removeAll();
				vueCreaQ.numQuestionActuelle += 1;
				vueCreaQ.nouvelle.setEnabled(false);
				vueCreaQ.invisible.setSelected(true);
				vueCreaQ.fond2.remove(vueCreaQ.placement_erreur_suivant);
				vueCreaQ.fond2.remove(vueCreaQ.placement_erreur_reponse);
				vueCreaQ.loadNext();

				vueCreaQ.cpt = 0;
				vueCreaQ.listeReponses.setPreferredSize(new Dimension(550,vueCreaQ.cpt));
				vueCreaQ.listeReponses.repaint();
				
				vueCreaQ.stock = new ArrayList<JPanel>();

			} else {
				vueCreaQ.fond2.remove(vueCreaQ.placement_erreur);
				vueCreaQ.fond2.remove(vueCreaQ.placement_erreur_suivant);
				vueCreaQ.fond2.add(vueCreaQ.placement_erreur_reponse);
			}

		} else{
			vueCreaQ.fond2.remove(vueCreaQ.placement_erreur_reponse);
			vueCreaQ.fond2.remove(vueCreaQ.placement_erreur);
			vueCreaQ.fond2.add(vueCreaQ.placement_erreur_suivant);
		}

		System.out.println(vueCreaQ.numQuestionActuelle);
		try{
			if (!vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle).getEnonce().equals("")){
				ArrayList<ValPossible> liste = vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle).getPropositions();
				for (ValPossible v : liste){
					vueCreaQ.listeReponses.add(vueCreaQ.addElem(v,fenetre));
				}
				vueCreaQ.intituler.setText(vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle).getEnonce());
			}
		} catch(Exception ev){
			System.out.println(ev);
		};


		fenetre.refresh();
	}


	public void retourQuestion(ActionEvent e){
		Vue_concepteur_crea_question vueCreaQ = ((Vue_concepteur_crea_question)fenetre.vue);
		vueCreaQ.cpt = 0;
		vueCreaQ.listeReponses.setPreferredSize(new Dimension(550,vueCreaQ.cpt));

		if(vueCreaQ.numQuestionActuelle > 0){
			vueCreaQ.numQuestionActuelle -= 1;
			if(!vueCreaQ.qu.contient(vueCreaQ.quest.getEnonce())){
				vueCreaQ.qu.addQuestion(vueCreaQ.quest);
			}
			if (((Vue_concepteur_crea_question)this.fenetre.vue).qu.getQuestion(vueCreaQ.numQuestionActuelle).getIdTypeQuestion()!='l'){
				((Vue_concepteur_crea_question)this.fenetre.vue).loadPrevious();
				vueCreaQ.listeReponses.removeAll();
				vueCreaQ.quest = vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle);
				ArrayList<ValPossible> liste = vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle).getPropositions();
				for (ValPossible v : liste){
					vueCreaQ.listeReponses.add(vueCreaQ.addElem(v,fenetre));
				}
				vueCreaQ.intituler.setText(vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle).getEnonce());
			}
			else{
				((Vue_concepteur_crea_question)this.fenetre.vue).loadPrevious();
				vueCreaQ.listeReponses.removeAll();
				vueCreaQ.quest = vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle);
				vueCreaQ.intituler.setText(vueCreaQ.qu.getQuestion(vueCreaQ.numQuestionActuelle).getEnonce());
			}
		}

		
		fenetre.refresh();
		vueCreaQ.stock = new ArrayList<JPanel>();
	}
	public void actionSuprr(ActionEvent e){
		int cptJpanel = 1;
		for (JPanel l : ((Vue_concepteur_crea_question)fenetre.vue).stock){
			l.setName(Integer.toString(cptJpanel));
			cptJpanel+=1;
		}
		((Vue_concepteur_crea_question)fenetre.vue).quest.removeProposition(Integer.parseInt(((Component)e.getSource()).getParent().getName())-1);
		((Vue_concepteur_crea_question)fenetre.vue).listeReponses.remove(Integer.parseInt(((Component)e.getSource()).getParent().getName())-1);
		((Vue_concepteur_crea_question)fenetre.vue).stock.remove(Integer.parseInt(((Component)e.getSource()).getParent().getName())-1);
		((Vue_concepteur_crea_question)fenetre.vue).cpt -= 70;
		((Vue_concepteur_crea_question)fenetre.vue).listeReponses.repaint();
		((Vue_concepteur_crea_question)fenetre.vue).fenetre.refresh();
	}
}
