package controleur;

import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;
import base.*;

import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.JOptionPane;

public class Action_ajout_client_sondage implements ActionListener{
	Application app;
	Vue_concepteur_ajout_client_sondage fenetre;
	
	public Action_ajout_client_sondage(Vue_concepteur_ajout_client_sondage fenetre, JFrame app){
		super();
		this.fenetre = fenetre;
		this.app=(Application)app;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		boolean testCrea = this.testCreation();	
		if(testCrea){
			
			int a = Integer.parseInt(fenetre.tel.getText());
			Client c=new Client(RqtConcepteur.maxNumClient(fenetre.fen.connection)+1,fenetre.nom.getText(),fenetre.rue.getText(),
					fenetre.complRue.getText(),Integer.parseInt(fenetre.codeP.getText()),fenetre.ville.getText(),fenetre.tel.getText()
					,fenetre.mail.getText());
			RqtConcepteur.addClient(c, Application.connection);
			fenetre.fen.loadVue("concepteur_creation_sondage");

		}
	}
	private boolean testCreation(){
		boolean res = true;		
		if(fenetre.nom==null||fenetre.nom.getText().equals("Nom Société")){
			fenetre.error.setText("ERREUR : Veuillez entrer un nom de client.");
			res=false;
		}
		else if(fenetre.rue==null||fenetre.rue.getText().equals("Adresse")){
			fenetre.error.setText("ERREUR : Veuillez entrer une rue.");
			res=false;
		}
		else if(fenetre.codeP==null||fenetre.codeP.getText().equals("Code Postal")){
			fenetre.error.setText("ERREUR : Veuillez entrer un code postal.");
			res=false;
		}
		else if(fenetre.codeP.getText().length()<5){
			fenetre.error.setText("ERREUR : Veuillez entrer un code postal valide (5 chiffres).");
			res=false;
		}
		else if(fenetre.ville==null||fenetre.ville.getText().equals("Ville")){
			fenetre.error.setText("ERREUR : Veuillez entrer une ville.");
			res=false;
		}
		else if(fenetre.tel==null||fenetre.tel.getText().equals("Téléphone")){
			fenetre.error.setText("ERREUR : Veuillez entrer un numéro de téléphone.");
			res=false;			
		}
		else if(fenetre.tel.getText().length()<10 || !fenetre.tel.getText().substring(0,1).equals("0")){
			fenetre.error.setText("ERREUR : Veuillez entrer un numéro de téléphone valide (10 chiffres et commençant par un 0).");
			res=false;
		}
		else if(fenetre.mail==null||fenetre.mail.getText().equals("Email")){
			fenetre.error.setText("ERREUR : Veuillez entrer une adresse mail.");
			res=false;
		}
		app.refresh();		
		return res;
	}
}