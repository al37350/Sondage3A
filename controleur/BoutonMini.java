package controleur;

import javax.swing.*;
import java.awt.*;

public abstract class BoutonMini extends JButton{
	public BoutonMini(JFrame fenetre, Icon image, String action, Color couleur){
		super(image);
		
		this.addActionListener(new ActionBouton(fenetre, action));

		this.setBackground(couleur);
		this.setPreferredSize(new Dimension(30, 30));
		this.setContentAreaFilled(false);
		this.setFocusPainted(false);
	}
}