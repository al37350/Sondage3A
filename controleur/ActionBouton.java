package controleur;

import Modele.*;
import base.*;
import sondeur.*;
import concepteur.*;

import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.JOptionPane;


public class ActionBouton implements ActionListener{
	private String nom;
	private Application fenetre;

	public ActionBouton(JFrame fenetre, String nom){
		super();
		this.nom = nom;
		this.fenetre = (Application)fenetre;
	}

	public void actionPerformed(ActionEvent e){
		switch (this.nom){
			case "deconnecter" : this.displayDcDialog(e); break;
			case "decroche" : this.fenetre.loadVue("questionnaire"); break;
			case "ndecroche" : ((Vue_sondeur_accueil)fenetre.vue).loadNew(); break;
			
			case "annuler_concepteur" : this.displayCancelDialog(e, "concepteur"); break;
			case "annuler_sondeur" : this.displayCancelDialog(e, "sondeur"); break;
			case "annuler_analyste" : this.displayCancelDialog(e, "analyste"); break;

			case "retour_concepteur" : this.fenetre.loadVue("concepteur"); break;
			case "retour_sondeur" : this.fenetre.loadVue("sondeur"); break;
			case "retour_analyste" : this.fenetre.loadVue("analyste"); break;
			
		}
	}

	public void displayDcDialog(ActionEvent e){
		Object[] options = {"Oui", "Non"};
	    int n = JOptionPane.showOptionDialog(null, "Etes vous sur de vouloir vous déconnecter ?", "Deconnexion", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, (Icon)PicturesLibrary.deconnexion, options, options[1]);
	    if (n == 0){
			this.fenetre.loadVue("connexion");
		}
	}

	public void displayCancelDialog(ActionEvent e, String target){
		Object[] options = {"Oui", "Non"};
	    int n = JOptionPane.showOptionDialog(null, "Etes vous sur de vouloir annuler \nl'action en cours ?", "Annulation", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, (Icon)PicturesLibrary.annuler, options, options[1]);
	    if (n == 0){
	    	switch (target){
	    		case "concepteur": this.fenetre.loadVue("concepteur"); break;
	    		case "sondeur": this.fenetre.loadVue("sondeur"); break;
	    		case "analyste": this.fenetre.loadVue("analyste"); break;
	    	}
			
		}
	}
	public void displayAnnulClientDialog(ActionEvent e){
		Object[] options = {"Oui", "Non"}; 
		int n = JOptionPane.showOptionDialog(null, "Etes vous sur de vouloir supprimer ce client ?", "Suppression", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, (Icon)PicturesLibrary.miniAnnuler, options, options[1]);
	    if (n==0){
	    	RqtConcepteur.deleteClient(Vue_concepteur_modifierClient.cli.getNumero(),Application.connection);
		this.fenetre.loadVue("concepteur_edition_client");
	    }
	}
}