package controleur;

import Modele.*;
import base.*;
import sondeur.*;

import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.JOptionPane;

public class ActionBouton_sondeur extends ActionBouton{
	private String nom;
	private Application fenetre;

	public ActionBouton_sondeur(JFrame fenetre, String nom){
		super(fenetre, nom);
		this.nom = nom;
		this.fenetre = (Application)fenetre;
	}


	@Override
	public void actionPerformed(ActionEvent e){
		switch (this.nom){
			case "deconnecter" : this.displayDcDialog(e); break;
			case "decroche" : this.fenetre.loadVue("questionnaire"); break;
			case "ndecroche" : ((Vue_sondeur_accueil)fenetre.vue).loadNew(); break;

			case "suivant" : ((Vue_sondeur_sondage)this.fenetre.vue).loadNext(); break;
			case "precedent" : ((Vue_sondeur_sondage)this.fenetre.vue).loadPrevious(); break;
		}
	        
	}
}