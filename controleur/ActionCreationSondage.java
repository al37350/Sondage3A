package controleur;

import base.*;
import Modele.*;
import concepteur.*;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;


public class ActionCreationSondage implements ActionListener {
	Application fenetre;
	Vue_concepteur_creation_sondage vue;	

	public ActionCreationSondage(JFrame fenetre, Vue_concepteur_creation_sondage vue){
		super();		
		this.fenetre = (Application)fenetre;
		this.vue=vue;
	}

	public void actionPerformed(ActionEvent e){
		String nomB = ((JButton)e.getSource()).getName();
		JButton bouton = ((JButton)e.getSource());
		fenetre.refresh();				
		if(nomB.substring(0,nomB.length()-1).equals("lierClient")){			
			if(vue.clientSelected!=null){
				vue.clientSelected.setBackground(Color.WHITE);				
			}
			vue.listePanelClient.get(Integer.parseInt(nomB.substring(nomB.length()-1,nomB.length()))).setBackground(ColorLibrary.bleu);
			vue.clientSelected=vue.listePanelClient.get(Integer.parseInt(nomB.substring(nomB.length()-1,nomB.length())));
			vue.idClientSelect=vue.clients.get(Integer.parseInt(nomB.substring(nomB.length()-1,nomB.length()))).getNumero();
			vue.error.setText("");
			fenetre.refresh();
			}

		if(nomB.substring(0,nomB.length()-1).equals("lierPanel")){			
			if(vue.panelSelected!=null){
				vue.panelSelected.setBackground(Color.WHITE);				
			}
			vue.listePanelPanel.get(Integer.parseInt(nomB.substring(nomB.length()-1,nomB.length()))).setBackground(ColorLibrary.bleu);
			vue.panelSelected=vue.listePanelPanel.get(Integer.parseInt(nomB.substring(nomB.length()-1,nomB.length())));
			vue.idPanelSelect=vue.panels.get(Integer.parseInt(nomB.substring(nomB.length()-1,nomB.length()))).getId();
			vue.error.setText("");
			fenetre.refresh();
			}
		if(nomB.equals("valider")){	
			if(vue.nomSondage.getText().equals("")){
				vue.error.setText("ERREUR : Veuillez entrer un nom de sondage.");
			}
			else if(vue.clientSelected==null){
				vue.error.setText("ERREUR : Veuillez lier un client au sondage.");
			} else if(vue.panelSelected==null){
				vue.error.setText("ERREUR : Veuillez lier un panel au sondage.");
			} else{
				fenetre.loadVue("concepteur_creation_question");
			}
		}
		if(nomB.equals("creerClient")){
			fenetre.loadVue("concepteur_ajout_client_sondage");
		}
	}
}
