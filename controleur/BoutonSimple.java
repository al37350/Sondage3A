package controleur;

import javax.swing.*;
import java.awt.*;

public abstract class BoutonSimple extends JButton{
	public BoutonSimple(JFrame fenetre, Icon image, String action, String section, Color couleur){
		super(image);
		
		if (action.equals("deconnecter")){
			this.addActionListener(new ActionBouton(fenetre, action));
		} else if (action.equals("annuler") && !section.equals("concepteur_creation_sondage")){
			this.addActionListener(new ActionBouton(fenetre, action + "_" + section));
		} else if (action.equals("annuler") && section.equals("concepteur_creation_sondage")){
			this.addActionListener(new ActionBouton_concepteur(fenetre, "creation_sondage"));
		} else {
			switch(section){
				case ("concepteur"): this.addActionListener(new ActionBouton_concepteur(fenetre, action)); break;
				case ("sondeur"): this.addActionListener(new ActionBouton_sondeur(fenetre, action)); break;
				case ("analyste"): break;

			}
		}
		

		this.setBackground(couleur);
		// this.setPreferredSize(new Dimension(0, 125));
		this.setContentAreaFilled(false);
		this.setFocusPainted(false);
	}
}