package controleur;

import base.*;

import javax.swing.*;

public class BoutonSuivant extends BoutonSimple{
	public BoutonSuivant(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.suivant, "suivant", section, ColorLibrary.beige);
	}
}