package controleur;

import base.*;
import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;

import java.awt.event.*;
import java.awt.Component;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.Color;

public class RadioBoutonMathieu implements ItemListener{
	String nom;
	Application fenetre;
	Vue_concepteur_crea_question v;

	public RadioBoutonMathieu(String nom, JFrame fenetre, Vue_concepteur_crea_question v){
		this.nom=nom;
		this.fenetre = (Application)fenetre;
		this.v = v;
	}

	public void itemStateChanged(ItemEvent e){
		if (e.getStateChange() == ItemEvent.SELECTED){
			if (nom.equals("choix_libre")){
				v.nouvelle.setEnabled(false);
				for (JPanel j : v.stock){
					j.setBackground(new Color(180,180,180));
				}
				v.type = 'l';
			} else if (nom.equals("choix_mul")){
				v.nouvelle.setEnabled(true);
				for (JPanel j : v.stock){
					j.setBackground(new Color(255,255,255));
				}
				v.type = 'm';
			} else if (nom.equals("choix_class")){
				v.nouvelle.setEnabled(true);
				for (JPanel j : v.stock){
					j.setBackground(new Color(255,255,255));
				}
				v.type = 'c';
			} else if (nom.equals("choix_unique")){
				v.nouvelle.setEnabled(true);
				for (JPanel j : v.stock){
					j.setBackground(new Color(255,255,255));
				}
				v.type = 'u';
			} else if (nom.equals("choix_note")){
				v.nouvelle.setEnabled(true);
				for (JPanel j : v.stock){
					j.setBackground(new Color(255,255,255));
				}
				v.type = 'n';
			}
			fenetre.refresh();
		}
	}
}