package controleur;

import base.*;
import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;

import java.awt.event.*;
import java.awt.Component;
import java.awt.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.Color;
import Modele.*;
import java.util.ArrayList;

public class Action_Bouton_ClientEdit extends ActionBouton{
	String nom;
	Application fenetre;
	

	public Action_Bouton_ClientEdit(JFrame fenetre, String nom){
		super(fenetre,nom);
		this.nom = nom;
		this.fenetre=(Application)fenetre;
	}
	@Override
	public void actionPerformed(ActionEvent e){
		if (nom.equals("edit_client")){
			Vue_concepteur_modifierClient.cli=RqtConcepteur.findClient(Integer.parseInt(((Component)e.getSource()).getName()),Application.connection);
			fenetre.loadVue("concepteur_edit_client");
		}
		if (nom.equals("supprimer_client")){
			Vue_concepteur_modifierClient.cli=RqtConcepteur.findClient(Integer.parseInt(((Component)e.getSource()).getName()),Application.connection);
			this.displayAnnulClientDialog(e);
		}
	}
}