package controleur;

import base.*;

import javax.swing.*;

public class BoutonValider extends BoutonSimple{
	public BoutonValider(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.valider, "valider", section, ColorLibrary.beige);
	}
}