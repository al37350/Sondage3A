package controleur;

import Modele.*;
import concepteur.*;
import base.*;
import sondeur.*;

import javax.swing.*;
import java.awt.event.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

//classe de lecture des champs de textes
public class Focus_ModifClient implements FocusListener{
	Vue_concepteur_modif_client lab;
	//paramètre n passé pour les mêmes raisons que la classe Datareaders
	public Focus_ModifClient(Vue_concepteur_modif_client lab)
	{
		this.lab = lab;
	}

	//je déclenche un évènement seulement quand le composant perd le focus
	//c'est à dir que le curseur change de case
	public void focusLost(FocusEvent evenement)
	{
		Object Donnees = evenement.getSource();
		JTextField champ = (JTextField)Donnees;
		if(champ.equals(lab.nom) && lab.nom.getText().equals("")){
			lab.nom.setDocument(new PlainDocument());
			lab.nom.setText("Nom Société");
		}
		if(champ.equals(lab.rue) && lab.rue.getText().equals("")){
			lab.rue.setDocument(new PlainDocument());
			lab.rue.setText("Adresse");			
		}
		if(champ.equals(lab.complRue) && lab.complRue.getText().equals("")){
			lab.complRue.setDocument(new PlainDocument());
			lab.complRue.setText("Complément");
		}
		if(champ.equals(lab.codeP) && lab.codeP.getText().equals("")){
			lab.codeP.setDocument(new PlainDocument());
			lab.codeP.setText("Code Postal");
		}
		if(champ.equals(lab.ville) && lab.ville.getText().equals("")){
			lab.ville.setDocument(new PlainDocument());
			lab.ville.setText("Ville");
		}
		if(champ.equals(lab.tel) && lab.tel.getText().equals("")){
			lab.tel.setDocument(new PlainDocument());
			lab.tel.setText("Téléphone");
		}
		if(champ.equals(lab.mail) && lab.mail.getText().equals("")){
			lab.mail.setDocument(new PlainDocument());
			lab.mail.setText("Email");
		}
	}

	public void focusGained(FocusEvent evenement)
	{
		Object Donnees = evenement.getSource();
		JTextField champ = (JTextField)Donnees;
		if(champ.equals(lab.nom) && lab.nom.getText().equals("Nom Société")){
			lab.nom.setText("");
			lab.nom.setDocument(new JTextLimited(30));
		}
		if(champ.equals(lab.rue) && lab.rue.getText().equals("Adresse")){
			lab.rue.setText("");
			lab.rue.setDocument(new JTextLimited(30));			
		}
		if(champ.equals(lab.complRue) && lab.complRue.getText().equals("Complément")){
			lab.complRue.setText("");
			lab.complRue.setDocument(new JTextLimited(30));
		}
		if(champ.equals(lab.codeP) && lab.codeP.getText().equals("Code Postal")){
			lab.codeP.setText("");
			lab.codeP.setDocument(new JTextLimitedNum(5));
		}
		if(champ.equals(lab.ville) && lab.ville.getText().equals("Ville")){
			lab.ville.setText("");
			lab.ville.setDocument(new JTextLimited(30));
		}
		if(champ.equals(lab.tel) && lab.tel.getText().equals("Téléphone")){
			lab.tel.setText("");
			lab.tel.setDocument(new JTextLimitedNum(10));
		}
		if(champ.equals(lab.mail) && lab.mail.getText().equals("Email")){
			lab.mail.setText("");
			lab.mail.setDocument(new JTextLimited(30));
		}
	}
}
