package controleur;

import base.*;

import javax.swing.*;

public class BoutonDeconnexion extends BoutonSimple{
	public BoutonDeconnexion(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.deconnexion, "deconnecter", section, ColorLibrary.beige);
	}
}