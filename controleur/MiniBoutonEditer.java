package controleur;

import base.*;

import javax.swing.*;

public class MiniBoutonEditer extends BoutonMini{
	public MiniBoutonEditer(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.miniEditer, "editer_" + section, ColorLibrary.beige);
	}
}