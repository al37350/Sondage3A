package controleur;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;

public abstract class Ajout{

	public static int cpt = 0;

	static public JPanel addElem(String question, String numQuestion, JFrame fenetre){
		MiniBoutonEditer modif = new MiniBoutonEditer(fenetre, "éditer question");
		modif.addActionListener(new ActionBouton_concepteur(fenetre, "éditer_question"));
		modif.addActionListener(new ActionBouton_concepteur(fenetre, ""));
		JPanel crea = new JPanel();
		JLabel questions = new JLabel(numQuestion+". "+ question);
		crea.setLayout(new BorderLayout());
		crea.add(modif,"East");
		crea.add(questions,"Center");
		crea.setPreferredSize(new Dimension(500,30));
		crea.setBorder(new LineBorder(Color.BLACK,1));
		cpt+=40;
		return crea;
	}
}