package controleur;


import Modele.*;
import base.*;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;


public class ActionScroll implements AdjustmentListener {
	Application fenetre;

	public ActionScroll(JFrame fenetre){
		super();		
		this.fenetre = (Application)fenetre;
	}


	public void adjustmentValueChanged(AdjustmentEvent e){
		fenetre.refresh();
	}
}