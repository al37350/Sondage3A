package controleur;

import base.*;

import javax.swing.*;

public class BoutonPrecedent extends BoutonSimple{
	public BoutonPrecedent(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.precedent, "precedent", section, ColorLibrary.beige);
	}
}