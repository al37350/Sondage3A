package controleur;

import Modele.*;
import base.*;

import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.JOptionPane;

public class Connection implements ActionListener{
	JTextField id;
	JPasswordField mdp;
	JLabel status;
	JFrame fenetre;

	public Connection(JFrame fenetre, JTextField id, JPasswordField mdp, JLabel status){
		super();
		this.id = id;
		this.mdp = mdp;
		this.status = status;
		this.fenetre = fenetre;
	}

	public void actionPerformed(ActionEvent e){
		String pass = "";
		char [] passChar = mdp.getPassword();
		for (char c: passChar){
			pass += c;
		}
		
		Application.user = RqtCommunes.connectUser(id.getText(), pass, Application.connection);
		try{
			switch (Application.user.getIdRole()){
				case 1:	((Application)this.fenetre).loadVue("concepteur"); break;
				case 2:	((Application)this.fenetre).loadVue("sondeur"); break;
				case 3:	((Application)this.fenetre).loadVue("analyste"); break;
			}
		} catch (NullPointerException except){
			status.setText("<html><font color = red >Identifiants non reconnus</font></html>");
		}
	}
}