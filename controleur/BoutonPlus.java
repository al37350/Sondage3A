package controleur;

import base.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.*;

public class BoutonPlus extends JButton{
	public BoutonPlus(JFrame fenetre, String role){
		super(Application.plus);
		this.addActionListener(new ActionBouton(fenetre, role));

		this.setBackground(new Color(250, 235, 225, 0));
		this.setPreferredSize(new Dimension(125, 125));
		this.setContentAreaFilled(false);
		this.setFocusPainted(false);
	}
}