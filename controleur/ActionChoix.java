package controleur;

import base.*;
import Modele.*;
import analyste.*;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;


public class ActionChoix implements ActionListener {
	Application fenetre;
	Vue_analyste_accueil accueil;	

	public ActionChoix(JFrame fenetre, Vue_analyste_accueil acc){
		super();		
		this.fenetre = (Application)fenetre;
		this.accueil = acc;
	}

	public void actionPerformed(ActionEvent e){
		String nomB = ((JButton)e.getSource()).getName();
		JButton bouton = ((JButton)e.getSource());		
		if(nomB=="analyser"){
			if(accueil.selected==null){
				accueil.error.setText("ERREUR : Veuillez sélectionner un sondage.");
			} else{
				Vue_analyste_accueil.q = accueil.getQuestionnaire(Integer.parseInt(accueil.getSelected().getName()));
				fenetre.loadVue("analyste_question");
			}


			// else{
			// 	try{
			// 		fenetre.remove(fenetre.vue);
			// 	} catch (Exception exce){}
			// 		fenetre.add(new Vue_analyste_question(fenetre, accueil.getQuestionnaire(Integer.parseInt(accueil.getSelected().getName()))));		
			// 	}
			// }

		} else {
			if(accueil.getSelected()!=null){
				accueil.getSelected().setBackground(Color.WHITE);				
			}
			accueil.error.setText("");
			bouton.setBackground(new Color(186, 234, 250, 100));
			accueil.setSelected(bouton);
			}
		fenetre.refresh();	
		}
		
	}

