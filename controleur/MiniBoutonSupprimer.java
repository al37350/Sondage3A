package controleur;

import base.*;

import javax.swing.*;

public class MiniBoutonSupprimer extends BoutonMini{
	public MiniBoutonSupprimer(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.miniAnnuler, "deconnection", ColorLibrary.beige);
	}
}