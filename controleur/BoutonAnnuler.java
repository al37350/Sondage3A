package controleur;

import base.*;

import javax.swing.*;

public class BoutonAnnuler extends BoutonSimple{
	public BoutonAnnuler(JFrame fenetre, String section){
		super(fenetre, PicturesLibrary.annuler, "annuler", section, ColorLibrary.bleu);
	}
}