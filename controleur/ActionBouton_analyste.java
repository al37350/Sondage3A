package controleur;

import Modele.*;
import base.*;
import analyste.*;

import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;
import javax.swing.JOptionPane;

public class ActionBouton_analyste extends ActionBouton{
	private String nom;
	private Application fenetre;

	public ActionBouton_analyste(JFrame fenetre, String nom){
		super(fenetre, nom);
		this.nom = nom;
		this.fenetre = (Application)fenetre;
	}


	@Override
	public void actionPerformed(ActionEvent e){
		switch (this.nom){
			case "suivant" :
				if(((Vue_analyste_question)this.fenetre.vue).numQuestion<((Vue_analyste_question)this.fenetre.vue).q.getNbQuestions()-1){
					((Vue_analyste_question)this.fenetre.vue).numQuestion+=1;
				}
				fenetre.loadVue("analyste_question");
				this.fenetre.refresh();
				break;
			case "precedent" :
				if(((Vue_analyste_question)this.fenetre.vue).numQuestion>0){
					((Vue_analyste_question)this.fenetre.vue).numQuestion-=1;
				}
				fenetre.loadVue("analyste_question");
				this.fenetre.refresh();
				break;
		}
	        
	}
}