package controleur;

import Modele.*;
import base.*;
import sondeur.*;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

public class JTextLimitedNum extends PlainDocument {
    private int taille;
 
    public JTextLimitedNum(int taille){
        super();
        this.taille=taille;
     
    }
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException{
        try{
            Integer.parseInt(str);
            if(offset<=taille-1){
                super.insertString(offset, str, attr);
            }
        }
        catch(Exception e){
            //..On ne fait rien
        }
    }
}