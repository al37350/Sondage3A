drop table Appartenance;
drop table Reponse;
drop table Question;
drop table AvoirRepondu;
drop table GererSondage;
drop table Utilisateur;
drop table Sondage;
drop table Panel;
drop table Societe;
drop table Sonde;


create table Societe(
	numInterne Number(10) primary key,
	raisonSocial Varchar(20),
	coordPostales Varchar(30),
	coordTel Number(10),
	coordMail Varchar(50)
);

create table Sonde(
	idSonde Number(10) primary key,
	nomS Varchar(10),
	prenomS Varchar(10),
	telS Number(10),
	codePostal Number(5),
	dateNaissance date
);

create table Panel(
	idPanel Number(10) primary key,
	nomPanel Varchar(50),
	nbPersonnes Number (4)
);

create table Sondage(
	numSond Number(10) primary key,
	nomSond Varchar(15),
	type Varchar(15),
	idPanel Number(10),
	numInterne Number(10),
	foreign key (idPanel) references Panel(idPanel) on delete cascade,
	foreign key (numInterne) references Societe(numInterne) on delete cascade
);

create table AvoirRepondu(
	numInterne Number(10),
	idSonde Number(10),
	statut Number(1),
	primary key(numInterne,idSonde),
	foreign key (numInterne) references Societe(numInterne) on delete cascade,
	foreign key (idSonde) references Sonde(idSonde) on delete cascade
);

create table Utilisateur(
	login Varchar(10) primary key,
	nomUti Varchar(10),
	prenomUti Varchar(10),
	motDePasse Varchar(15),
	role Varchar(15)
);

create table GererSondage(
	login Varchar(10),
	numSond Number(10),
	primary key(login,numSond),
	foreign key (login) references Utilisateur(login) on delete cascade,
	foreign key (numSond) references Sondage(numSond) on delete cascade
);

create table Question(
	idQuestion Number(10) primary key,
	nomQuestion Varchar(15),
	typeQuestion Varchar(15),
	intituleQuestion Varchar(100),
	numSond Number(10),
	foreign key (numSond) references Sondage(numSond) on delete cascade
);

create table Reponse(
	idReponse Number(10),
	intituleReponse Varchar(50),
	VilleS Varchar(50),
	categorieS Varchar(20),
	sexeS Varchar(1),
	ageS Number(3),
	importance Number(1),
	idQuestion Number(10),
	primary key(idReponse,idQuestion),
	foreign key (idQuestion) references Question(idQuestion) on delete cascade
);

create table Appartenance(
	idSonde Number(10),
	idPanel Number(10),
	primary key(idPanel,idSonde),
	foreign key (idSonde) references Sonde(idSonde) on delete cascade,
	foreign key (idPanel) references Panel(idPanel) on delete cascade
);
