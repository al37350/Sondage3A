
-- Requete 1 : La liste des questions du questionnaire numero xxx
select idQuestion, intituleQuestion from Question where numSond=xxx;

-- Requete 2 : La liste des personnes d'un panel qui n'ont pas encore repondu au questionnaire numero xxx
select nomS,prenomS from Sonde,AvoirRepondu,Panel,Appartenance,Sondage where Sonde.idSonde=Appartenance.idSonde and Panel.idPanel=Appartenance.idPanel and Sonde.idSonde=AvoirRepondu.idSonde and Sondage.idPanel=Panel.idPanel and Sondage.numSond=xxx and AvoirRepondu.statut=0;

-- Requete 3 : La liste des reponses a la question yyy du questionnaire xxx
select idReponse,intituleReponse,importance from Reponse natural join Question where idQuestion=yyy and numSond=xxx;
