package concepteur;

import Modele.*;
import base.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;


public class Vue_concepteur_modif_questionnaire extends Vue_globale{

	Ajout a;	
	public ArrayList<Client> clients;
	public Questionnaire sondage;
	public ArrayList<Question> questions;
	public ArrayList<PanelSonde> panels;
	public static JTextField nomSondage;
	public JPanel clientSelected;
	public JPanel panelSelected;
	public ArrayList<JPanel> listePanelPanel;
	public ArrayList<JPanel> listePanelClient;	
	public static int idClientSelect;
	public static int idPanelSelect;
	public JLabel error;

	public Vue_concepteur_modif_questionnaire(JFrame fenetre, Questionnaire sondage){
		super(fenetre, "doubleB", "basic", "basic", "concepteur");
		this.sondage=sondage;

		listePanelClient = new ArrayList<JPanel>();
		listePanelPanel = new ArrayList<JPanel>();
		

		panels = PanelSonde.getAllPanel(Application.connection);
		clients = RqtConcepteur.allClient(Application.connection);
		questions = RqtCommunes.getAllQuestion(sondage.getidQ(), Application.connection);

		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.CENTER);
		layout.setVgap(20);

		FlowLayout layout2 = new FlowLayout();
		layout2.setAlignment(FlowLayout.CENTER);
		layout2.setHgap(300);
		layout2.setVgap(20);

		FlowLayout layout3 = new FlowLayout();
		layout3.setAlignment(FlowLayout.CENTER);
		layout3.setVgap(20); 
		layout3.setHgap(50);

		FlowLayout layout4 = new FlowLayout();		
		layout4.setVgap(5); 
		layout4.setAlignment(FlowLayout.LEFT);

		FlowLayout layout5 = new FlowLayout();				
		layout5.setAlignment(FlowLayout.LEADING);

		FlowLayout layout6 = new FlowLayout();
		layout6.setAlignment(FlowLayout.CENTER);
		layout6.setVgap(25); 


		
		// titre principal de la fenetre
		JLabel titre = new JLabel("Modification : " + sondage.getTitre());
		titre.setFont(FontLibrary.fontTitre);
		
		
		// JPanel principal
		JPanel fond = new JPanel();
		fond.setLayout(new FlowLayout());

		// JPanel secondaire
		JPanel fond2 = new JPanel();
		fond2.setLayout(new GridLayout(2,1));


		// JPanel pour centrer titre
		JPanel centre_titre = new JPanel(layout);
		centre_titre.add(titre);

		JPanel lierClientPrinc = new JPanel();
		lierClientPrinc.setLayout(new BorderLayout());
		lierClientPrinc.setBackground(ColorLibrary.beige);

		JPanel lierClient = new JPanel();		
		lierClient.setLayout(layout6);					
		lierClient.setPreferredSize(new Dimension(360,68*(clients.size()+1)));
		lierClient.setBackground(ColorLibrary.beige);


		JScrollPane jspClient = new JScrollPane();
		jspClient.setViewportView(lierClient);		
		jspClient.setBorder(new LineBorder(Color.black,2));
		jspClient.setPreferredSize(new Dimension(390,170));
		jspClient.getVerticalScrollBar().setUnitIncrement(10);
		jspClient.setBackground(ColorLibrary.beige);
		jspClient.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));

		JLabel labelClient = new JLabel("Client lié :");
		labelClient.setFont(FontLibrary.fontLabel);
		labelClient.setBackground(ColorLibrary.beige);

		JPanel panelLabelClient = new JPanel();
		panelLabelClient.setLayout(layout4);
		panelLabelClient.add(labelClient);
		panelLabelClient.setBackground(ColorLibrary.beige);		

		JPanel lierPanelPrinc = new JPanel();
		lierPanelPrinc.setLayout(new BorderLayout());
		lierPanelPrinc.setBackground(ColorLibrary.beige);

		JPanel lierPanel = new JPanel();
		lierPanel.setLayout(layout6);			
		lierPanel.setPreferredSize(new Dimension(360,68*(panels.size()+1)));
		lierPanel.setBackground(ColorLibrary.beige);

		JLabel labelPanel = new JLabel("Panel lié :");
		labelPanel.setFont(FontLibrary.fontLabel);
		labelPanel.setBackground(ColorLibrary.beige);

		JPanel panelLabelPanel = new JPanel();
		panelLabelPanel.setLayout(layout4);
		panelLabelPanel.add(labelPanel);
		panelLabelPanel.setBackground(ColorLibrary.beige);	
		

		JScrollPane jspPanel = new JScrollPane();
		jspPanel.setViewportView(lierPanel);
		jspPanel.setBorder(new LineBorder(Color.black,2));			
		jspPanel.setPreferredSize(new Dimension(390,170));
		jspPanel.getVerticalScrollBar().setUnitIncrement(10);
		jspPanel.setBackground(ColorLibrary.beige);
		jspPanel.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));

		ActionModifSondage acs = new ActionModifSondage(fenetre, this);
		JPanel client;
		JPanel bouton;
		JPanel labClient;		
		JLabel nom;
		JButton lier;

		GridLayout grid = new GridLayout(1,2);		
		grid.setHgap(40); 

		FlowLayout layout7 = new FlowLayout();		
		layout7.setHgap(40); 
		layout7.setVgap(12); 
		


		for(int i=0; i<clients.size();i++){
			client = new JPanel();
			client.setLayout(grid);
			client.setPreferredSize(new Dimension(340,50));
			client.setBorder(new LineBorder(Color.black,1));
			client.setBackground(Color.WHITE);	

			nom = new JLabel(clients.get(i).getRaisonSociale());
			nom.setFont(FontLibrary.fontLabel);

			labClient = new JPanel();
			labClient.setLayout(layout7);
			labClient.setBackground(new Color(0,0,0,0));	
			labClient.add(nom);

			
			client.add(labClient);

			lier = new JButton("Lier");					
			lier.addActionListener(acs);
			lier.setName("lierClient"+Integer.toString(i));
			lier.setBackground(new Color(220,220,220,50));		
			lier.setBorder(new LineBorder(Color.black,1));
			lier.setPreferredSize(new Dimension(120,39));
			lier.setFont(FontLibrary.fontLabel);

			bouton = new JPanel();
			bouton.setLayout(new FlowLayout());
			bouton.setBackground(new Color(0,0,0,0));

			bouton.add(lier);

			client.add(bouton);

			lierClient.add(client);
			listePanelClient.add(client);
		}
		

		JPanel panel;		
		JPanel labPanel;		
		


		for(int i=0; i<panels.size();i++){
			panel = new JPanel();
			panel.setLayout(grid);
			panel.setPreferredSize(new Dimension(340,50));
			panel.setBorder(new LineBorder(Color.black,1));
			panel.setBackground(Color.WHITE);	

			nom = new JLabel(panels.get(i).getName());
			nom.setFont(FontLibrary.fontLabel);

			labPanel = new JPanel();
			labPanel.setLayout(layout7);
			labPanel.setBackground(new Color(0,0,0,0));	
			labPanel.add(nom);

			
			panel.add(labPanel);

			lier = new JButton("Lier");					
			lier.addActionListener(acs);
			lier.setName("lierPanel"+Integer.toString(i));
			lier.setBackground(new Color(220,220,220,50));		
			lier.setBorder(new LineBorder(Color.black,1));
			lier.setPreferredSize(new Dimension(120,39));
			lier.setFont(FontLibrary.fontLabel);

			bouton = new JPanel();
			bouton.setLayout(new FlowLayout());
			bouton.setBackground(new Color(0,0,0,0));

			bouton.add(lier);

			panel.add(bouton);

			lierPanel.add(panel);
			listePanelPanel.add(panel);
		}

		JPanel lierPrinc = new JPanel();
		lierPrinc.setLayout(layout3);
		lierPrinc.setBackground(ColorLibrary.beige);


		JLabel labelParam = new JLabel("<html>Paramètres du<br>sondage :</html>");
		labelParam.setFont(FontLibrary.fontLabel);
		labelParam.setPreferredSize(new Dimension(150,150));
		

		JPanel panelParam = new JPanel();
		panelParam.setLayout(layout5);
		panelParam.add(labelParam);
		panelParam.setBackground(ColorLibrary.beige);

		lierClientPrinc.add(panelLabelClient,"North");
		lierClientPrinc.add(jspClient,"Center");
		lierPanelPrinc.add(panelLabelPanel,"North");
		lierPanelPrinc.add(jspPanel,"Center");

		lierPrinc.add(panelParam);
		lierPrinc.add(lierClientPrinc);
		lierPrinc.add(lierPanelPrinc);
		lierPrinc.setBackground(ColorLibrary.beige);


		// Partie question :

		JPanel lierQuestion = new JPanel();
		lierQuestion.setLayout(new BorderLayout());
		lierQuestion.setBackground(ColorLibrary.beige);

		
		JPanel modifQuestion = new JPanel();		
		modifQuestion.setLayout(layout6);					
		modifQuestion.setPreferredSize(new Dimension(730,68*(2)));
		modifQuestion.setBackground(ColorLibrary.beige);


		JScrollPane jspQuestion = new JScrollPane();
		jspQuestion.setViewportView(modifQuestion);		
		jspQuestion.setBorder(new LineBorder(Color.black,2));
		jspQuestion.setPreferredSize(new Dimension(760,250));
		jspQuestion.getVerticalScrollBar().setUnitIncrement(10);
		jspQuestion.setBackground(ColorLibrary.beige);
		jspQuestion.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));

		JLabel labelQuestion = new JLabel("Questions :");
		labelQuestion.setFont(FontLibrary.fontLabel);
		labelQuestion.setBackground(ColorLibrary.beige);

		JPanel panelLabelQuestion = new JPanel();
		panelLabelQuestion.setLayout(layout4);
		panelLabelQuestion.add(labelQuestion);
		panelLabelQuestion.setBackground(ColorLibrary.beige);	

		
		JLabel labelNomSond = new JLabel("Nom du sondage :");
		labelNomSond.setFont(FontLibrary.fontLabel);

	
		nomSondage = new JTextField(sondage.getTitre(),35);
		nomSondage.setDocument(new JTextLimited(30));
		nomSondage.setFont(FontLibrary.fontTextField);
		nomSondage.setText(sondage.getTitre());


		JPanel nomSond = new JPanel();
		titre.setLayout(layout);

		JPanel question;	
		JPanel labQuestion;
		JButton modifier;

		GridLayout grid2 = new GridLayout(1,2);		
		grid2.setHgap(0);

		FlowLayout flowBouton = new FlowLayout();
		flowBouton.setHgap(110);
	

		for(int i=0; i<questions.size();i++){
			question = new JPanel();
			question.setLayout(grid2);
			question.setPreferredSize(new Dimension(730,50));
			question.setBorder(new LineBorder(Color.black,1));
			question.setBackground(Color.WHITE);	

			nom = new JLabel(questions.get(i).getEnonce());
			nom.setFont(FontLibrary.fontLabel);

			labQuestion = new JPanel();
			labQuestion.setLayout(layout7);
			labQuestion.setBackground(new Color(0,0,0,0));	
			labQuestion.add(nom);

			
			question.add(labQuestion);

			modifier = new JButton("Modifier");					
			modifier.addActionListener(acs);
			modifier.setName("modifierClient"+Integer.toString(i));
			modifier.setBackground(new Color(220,220,220,50));		
			modifier.setBorder(new LineBorder(Color.black,1));
			modifier.setPreferredSize(new Dimension(110,39));
			modifier.setFont(FontLibrary.fontLabel);

			bouton = new JPanel();
			bouton.setLayout(flowBouton);
			bouton.setBackground(new Color(0,0,0,0));

			bouton.add(modifier);

			question.add(bouton);

			modifQuestion.add(question);
		}
		

		lierQuestion.add(panelLabelQuestion,"North");
		lierQuestion.add(jspQuestion,"Center");

		nomSond.add(labelNomSond);
		nomSond.add(nomSondage);
		nomSond.setBackground(ColorLibrary.beige);
		
		error = new JLabel();
		error.setFont(FontLibrary.fontErreur);
		error.setForeground(Color.RED);

		

		// Ajout du titre et de client lie et panel lie au JPanel secondaire
		
	

		// Ajout de fond2 et la liste de question au JPanel principal
		fond.add(centre_titre);
		fond.add(nomSond);		
		fond.add(lierPrinc);
		fond.add(lierQuestion);
		fond.add(error);

		// Application du JPanel principal a la fenetre
		this.add(fond);
		
	}
}



