package concepteur;

import Modele.*;
import base.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;

public class Vue_concepteur_edition_questionnaire extends Vue_globale{

	int cpt = 0;

	public Vue_concepteur_edition_questionnaire(JFrame fenetre){
		super(fenetre, "annuler", "basic", "basic", "concepteur");
				
		// JPanel placement
		JPanel fond = new JPanel();
		FlowLayout layout_fond = new FlowLayout();
		layout_fond.setVgap(100);
		fond.setLayout(layout_fond);

		JPanel contient_tout = new JPanel();
		contient_tout.setLayout(new BorderLayout());

		// JPanel pour les questions
		JPanel listeSondages = new JPanel();
		
		// JLabel questionnaire
		JLabel questionnaire = new JLabel("Questionnaire :");
		questionnaire.setFont(FontLibrary.fontLabel);

		// JPanel pour le label
		JPanel label_quest = new JPanel();
		label_quest.setLayout(new BorderLayout());
		label_quest.add(questionnaire,"West");

		// Scroll bar pour la List
		JScrollPane scroll = new JScrollPane(listeSondages);
		scroll.setPreferredSize(new Dimension(750,500));
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.getVerticalScrollBar().setUnitIncrement(10);

		// Info utilisateur
		GridLayout layoutDroite = new GridLayout(2, 1);
		layoutDroite.setVgap(-30);
		droite.setLayout(layoutDroite);
		JLabel np = new JLabel("     " + Application.user.getPrenom() + " " + Application.user.getNom());
		np.setFont(FontLibrary.fontLabel);
		JLabel role = new JLabel("     " + Application.user.getRole());
		role.setFont(FontLibrary.fontLabel);

		droite.add(np);
		droite.add(role);


		contient_tout.add(label_quest,"North");
		contient_tout.add(scroll,"Center");

		listeSondages.add(this.addElem("titre","1"));

		listeSondages.setPreferredSize(new Dimension(750,cpt));

		fond.add(contient_tout,"Center");
		this.add(fond);
	}

		// override 
		public JPanel addElem(String nomSondage, String numSondage){
		MiniBoutonSupprimer suppr = new MiniBoutonSupprimer(fenetre, "supprQuestion");
		suppr.addActionListener(new ActionBouton_concepteur(fenetre, "supprimer_question"));
		MiniBoutonEditer editer = new MiniBoutonEditer(fenetre, "editQuestion");
		editer.addActionListener(new ActionBouton_concepteur(fenetre, "éditer_question"));

		JPanel bouton = new JPanel();
		bouton.add(editer);
		bouton.add(suppr);
		JPanel crea = new JPanel();
		JLabel num = new JLabel(numSondage+".");
		JLabel sondage = new JLabel(nomSondage);
		JPanel placer_txt = new JPanel();
		JPanel placer_nb = new JPanel();
		placer_txt.add(sondage);
		placer_nb.add(num);
		crea.setLayout(new BorderLayout());
		crea.add(placer_nb,"West");
		crea.add(placer_txt,"Center");
		crea.add(bouton,"East");
		crea.setPreferredSize(new Dimension(650,40));
		crea.setBorder(new LineBorder(Color.BLACK,1));
		cpt+=45;
		return crea;
		
	}
}
