package concepteur;

import base.*;
import Modele.*;
import concepteur.*;
import sondeur.*;
import analyste.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;

public class Vue_concepteur_edition_questionnaire_liste extends Vue_globale{
	public static ArrayList<Questionnaire> listeSond;
	public static Client c;
	public static Questionnaire q;
	public Questionnaire sondageSelect;

	public Vue_concepteur_edition_questionnaire_liste(JFrame fenetre){
		super(fenetre, "annuler", "basic", "basic", "concepteur");

		listeSond = RqtConcepteur.getAllQuestionnaire(Application.connection);

		Font r = new Font("Calibri", Font.PLAIN, 20);

		FlowLayout layoutPrinc = new FlowLayout();
		layoutPrinc.setAlignment(FlowLayout.CENTER);
		layoutPrinc.setVgap(55);

		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.CENTER);
		layout.setVgap(40);

		JPanel princ = new JPanel();
		princ.setLayout(layoutPrinc);
		princ.setBackground(ColorLibrary.beige);

		FlowLayout layout2 = new FlowLayout();
		layout2.setAlignment(FlowLayout.CENTER);
		layout2.setHgap(300);
		layout2.setVgap(20);
		
		JPanel titre = new JPanel();
		titre.setLayout(layout);
		titre.setBackground(ColorLibrary.beige);

		JPanel quest = new JPanel();
		quest.setLayout(layout2);
		quest.setBackground(ColorLibrary.beige);
		
		// Création du label questionnaire
		JLabel labelQuest = new JLabel("Sondages :");
		labelQuest.setFont(FontLibrary.fontLabel);
		quest.add(labelQuest);

		// Création du panel de choix d'un sondage à analyser
		FlowLayout layout3 = new FlowLayout();
		layout3.setAlignment(FlowLayout.CENTER);
		layout3.setVgap(25); 

		FlowLayout layout4 = new FlowLayout();
		layout4.setAlignment(FlowLayout.CENTER);
		layout4.setVgap(14); 

		JPanel choix = new JPanel();
		choix.setLayout(layout3);			
		choix.setBorder(new LineBorder(Color.black,2));		
		choix.setPreferredSize(new Dimension(1000,65*(listeSond.size()+1)));
		choix.setBackground(ColorLibrary.beige);

		for(int i=0;i<listeSond.size();i++){
			if (listeSond.get(i).getEtat().equals("C")){
				BorderLayout layout5 = new BorderLayout();
				JPanel panel = new JPanel(new BorderLayout());
				JPanel panel2 = new JPanel(layout4);
				JLabel nomSond = new JLabel("         "+listeSond.get(i).getTitre(), SwingConstants.CENTER);
				nomSond.setFont(FontLibrary.fontLabel);
				panel2.setName(""+(i+1));			
				panel.setBackground(Color.WHITE);							
				panel2.setBackground(Color.WHITE);		
				panel.setBorder(new LineBorder(Color.black,1));
				panel.setPreferredSize(new Dimension(900,50));
				panel.add("Center",nomSond);				
				MiniBoutonEditer editer = new MiniBoutonEditer(fenetre, "editer");
				editer.addActionListener(new ActionBouton_EditionSondage(fenetre, "modif_sondage",this));
				MiniBoutonSupprimer suppr = new MiniBoutonSupprimer(fenetre, "supprimer");
				suppr.addActionListener(new ActionBouton_EditionSondage(fenetre, "modif_supprimer", this));
				panel2.add(editer);
				panel2.add(suppr);
				panel.add("East", panel2);
				choix.add(panel);
			}
		}

		JScrollPane jsp = new JScrollPane();
		jsp.setViewportView(choix);		
		jsp.setPreferredSize(new Dimension(1020,500));
		jsp.getVerticalScrollBar().setUnitIncrement(10);
		jsp.setBackground(ColorLibrary.beige);
		jsp.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));


		JPanel choixQuest = new JPanel();
		choixQuest.setLayout(new BorderLayout());
		choixQuest.add(labelQuest,"North");
		choixQuest.add(jsp,"Center");
		choixQuest.setBackground(ColorLibrary.beige);
	
		princ.add(choixQuest);

		this.add(princ,"Center");
	}
}


