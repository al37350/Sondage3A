package concepteur;

import Modele.*;
import base.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;

public class Vue_concepteur_ajout_client_sondage extends Vue_globale{
	public JTextField nom;
	public JTextField rue;
	public JTextField complRue;
	public JTextField codeP;
	public JTextField ville;
	public JTextField tel;
	public JTextField mail;
	public Application fen;
	public JLabel error;


	public Vue_concepteur_ajout_client_sondage(JFrame fenetre){
		super(fenetre, "annuler", "basic", "basic", "concepteur_creation_sondage");
		
		fen= (Application)fenetre;
		
		fen=(Application)fenetre;
		
		//initialisation des jPanel
		JPanel global=new JPanel(new FlowLayout());
		global.setPreferredSize(new Dimension(1100,800));
		global.setBackground(ColorLibrary.beige);

		GridLayout layout = new GridLayout(5,1);
		layout.setVgap(0);

		FlowLayout layout2 = new FlowLayout();
		layout2.setHgap(20);

		GridLayout layout3 = new GridLayout(2,1);
		layout3.setVgap(30);

		
		JPanel global2=new JPanel(layout);
		global2.setBackground(ColorLibrary.beige);

		JPanel raisonSociale = new JPanel(layout2);
		raisonSociale.setBackground(ColorLibrary.beige);

		JPanel adresse1 = new JPanel(layout2);
		adresse1.setBackground(ColorLibrary.beige);

		JPanel adresse2 = new JPanel(layout2);
		adresse2.setBackground(ColorLibrary.beige);

		JPanel addr = new JPanel(layout3);
		addr.setBackground(ColorLibrary.beige);

		JPanel contact1 = new JPanel(layout2);
		contact1.setBackground(ColorLibrary.beige);

		JPanel bouton = new JPanel();
		bouton.setLayout(new GridLayout(1,1));
		bouton.setPreferredSize(new Dimension(115,0));
		bouton.setBackground(ColorLibrary.beige);
		
		//initialisation des label				
		JLabel titre = new JLabel("Création client");
		titre.setFont(FontLibrary.fontTitre);
		titre.setHorizontalAlignment(JLabel.CENTER);

		JLabel raisonSoc= new JLabel("<html>Raison<br>Sociale</html>");
		raisonSoc.setFont(FontLibrary.fontLabel);

		JLabel adr = new JLabel("Adresse");
		adr.setFont(FontLibrary.fontLabel);
		
		JLabel cont= new JLabel("Contact");
		cont.setFont(FontLibrary.fontLabel);
		
		
		JLabel vide= new JLabel("            ");
		vide.setFont(FontLibrary.fontLabel);

		JPanel erreur = new JPanel(new FlowLayout());
		erreur.setBackground(ColorLibrary.beige);

		error = new JLabel("");
		error.setFont(FontLibrary.fontErreur);
		error.setForeground(Color.RED);

		erreur.add(error);
	
		
		
		// initilisation des champ de texte
		Dimension jt = new Dimension(0, 50);
		
		nom=new JTextField("Nom Société",70);	
		rue=new JTextField("Adresse",40);
		complRue=new JTextField("Complément",27);
		codeP=new JTextField("Code Postal",27);
		ville=new JTextField("Ville",40);
		tel=new JTextField("Téléphone",27);
		mail=new JTextField("Email",40);
		
		nom.setPreferredSize(jt);
		rue.setPreferredSize(jt);
		complRue.setPreferredSize(jt);
		codeP.setPreferredSize(jt);
		ville.setPreferredSize(jt);
		tel.setPreferredSize(jt);
		mail.setPreferredSize(jt);
		
		// nom.setText(cli.getRaisonSociale());
		// rue.setText(cli.getAdresse1());
		// complRue.setText(cli.getAdresse2());
		// codeP.setText(""+cli.getCodePostal());
		// ville.setText(cli.getVille());
		// tel.setText(""+cli.getTelephone());
		// mail.setText(cli.getEmail());

		nom.setHorizontalAlignment(JTextField.CENTER);
		rue.setHorizontalAlignment(JTextField.CENTER);
		complRue.setHorizontalAlignment(JTextField.CENTER);
		codeP.setHorizontalAlignment(JTextField.CENTER);
		ville.setHorizontalAlignment(JTextField.CENTER);
		tel.setHorizontalAlignment(JTextField.CENTER);
		mail.setHorizontalAlignment(JTextField.CENTER);
		

		Focus_ajout_client_sondage monListener = new Focus_ajout_client_sondage(this);

		nom.addFocusListener(monListener);
		rue.addFocusListener(monListener);
		complRue.addFocusListener(monListener);
		codeP.addFocusListener(monListener);
		ville.addFocusListener(monListener);
		tel.addFocusListener(monListener);
		mail.addFocusListener(monListener);

		// initialisation des bouton
		BoutonValider valid = new BoutonValider(fenetre,"Valider");
		valid.setName("valid");
		valid.addActionListener(new Action_ajout_client_sondage(this,fenetre));
		valid.setBackground(ColorLibrary.beige);
		//ajout des contents au panel
		
		raisonSociale.add(raisonSoc);
		raisonSociale.add(nom);

		adresse1.add(adr);
		adresse1.add(rue);
		adresse1.add(complRue);

		adresse2.add(vide);
		adresse2.add(codeP);
		adresse2.add(ville);

		contact1.add(cont);
		contact1.add(tel);
		contact1.add(mail);

		addr.add(adresse1);
		addr.add(adresse2);

		global2.add(titre);
		global2.add(raisonSociale);
		global2.add(addr);
		global2.add(contact1);
		global2.add(erreur);


		global.add(global2);
		

		bouton.add(valid);

		this.add(bouton,"East");
		this.add(global,"West");
	}
}