package concepteur;

import Modele.*;
import base.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;

public class Vue_concepteur_crea_question extends Vue_globale{

	public char type;
	public ArrayList<JPanel> stock = new ArrayList<JPanel>();
	public JPanel listeReponses;
	public JButton nouvelle;
	public int progression, max;
	private JProgressBar progressBar;
	private JLabel legendeProgression;
	public JTextField intituler;
	public int cpt = 0;
	public JRadioButton choix_libre;
	private String nom;
	public int idClient;
	private int idPanel;
	public Questionnaire qu;
	public int numQuestionActuelle = 0;
	public Question quest;
	public JRadioButton invisible;
	public JPanel placement_erreur;
	public JPanel fond2;
	public JPanel placement_erreur_suivant;
	public JPanel placement_erreur_reponse;
	private int cptRep = 0;


	public Vue_concepteur_crea_question(JFrame fenetre){
		super(fenetre, "doubleB", "basic", "progressBar", "concepteur");
		this.nom= Vue_concepteur_creation_sondage.nomSondage.getText();
		this.idClient= Vue_concepteur_creation_sondage.idClientSelect;
		this.idPanel= Vue_concepteur_creation_sondage.idPanelSelect;

		// creation d'un questionnaire
		qu = new Questionnaire(RqtConcepteur.getNumNextQuestionnaire(Application.connection),nom,"C",idPanel,RqtConcepteur.findClient(idClient,Application.connection));
		System.out.println(qu.getNbQuestions());
		// JPanel pour positionner le titre au centre
		JPanel posTitre = new JPanel();

		//JPanel total
		JPanel principal = new JPanel();
		principal.setLayout(new BorderLayout());

		// JPanel principal
		JPanel fond = new JPanel();
		fond.setLayout(new GridLayout(2,1));

		// JPanel pour le titre et l'intitulé question
		fond2 = new JPanel();
		fond2.setLayout(new GridLayout(3,1));

		// JPanel pour la liste des réponses
		JPanel texte_et_reponse = new JPanel();
		texte_et_reponse.setLayout(new BorderLayout());

		// Contient le JLabel et le JTextField pour la question
		JPanel intituler_et_question = new JPanel();

		// Contient type question + reponse
		JPanel contient_choix_reponse = new JPanel();
		FlowLayout layoutchoix = new FlowLayout();
		layoutchoix.setHgap(50);
		contient_choix_reponse.setLayout(layoutchoix);

		// JLabel reponse + List reponse
		JPanel contient_reponse = new JPanel();

		// Contient radio Boutton type et bouton ajouter réponse
		JPanel choix_et_creation = new JPanel();
		BorderLayout layoutcreation = new BorderLayout();
		layoutcreation.setVgap(20);
		choix_et_creation.setLayout(layoutcreation);

		// JPanel pour JLabel type question et radio Button type
		JPanel border = new JPanel();
		BorderLayout layoutborder = new BorderLayout();
		layoutborder.setVgap(10);
		border.setLayout(layoutborder);
		border.setBorder(new LineBorder(Color.BLACK,2));

		// JPanel pour centrer le label type de question
		JPanel centrer_texte = new JPanel();

		// JLabel pour Type de question
		JLabel type_question = new JLabel("Type de question");
		type_question.setFont(FontLibrary.fontLabel);

		// JLabel avec le nom du questionnaire
		JLabel titre = new JLabel("Création du questionnaire : "+nom);
		titre.setFont(FontLibrary.fontTitre);

		// JLabel pour intitulé question
		JLabel question = new JLabel("Intitulé question : ");
		question.setFont(FontLibrary.fontLabel);

		// JLabel pour les réponses
		JLabel reponse = new JLabel("Réponses :");
		reponse.setFont(FontLibrary.fontLabel);

		//JLabel erreur 
		JLabel erreur_intituler = new JLabel("Merci de rentrer le nom question avant de rentrer pouvoir une réponse");
		erreur_intituler.setFont(FontLibrary.fontLabel);
		erreur_intituler.setForeground(new Color(255,0,0));

		//JPanel erreur
		placement_erreur = new JPanel();
		placement_erreur.add(erreur_intituler);

		//JLabel erreur suivant
		JLabel erreur_suivant = new JLabel("Merci de rentrer une question avant de passer a la question suivante");
		erreur_suivant.setFont(FontLibrary.fontLabel);
		erreur_suivant.setForeground(new Color(255,0,0));

		placement_erreur_suivant = new JPanel();
		placement_erreur_suivant.add(erreur_suivant);

		JLabel manque_reponse = new JLabel("Merci de rentrer au moins une réponse");
		manque_reponse.setFont(FontLibrary.fontLabel);
		manque_reponse.setForeground(new Color(255,0,0));

		placement_erreur_reponse = new JPanel();
		placement_erreur_reponse.add(manque_reponse);

		// JTextField pour rentrer la question
		intituler = new JTextField(50);
		intituler.setPreferredSize(new Dimension(100,25));

		// JPanel pour contenir les différentes réponses
		listeReponses = new JPanel();
		FlowLayout rep = new FlowLayout();
		listeReponses.setLayout(rep);
		rep.setVgap(25);

		// Scroll bar pour la list
		JScrollPane scroll = new JScrollPane(listeReponses);
		scroll.setPreferredSize(new Dimension(550,250));
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.getVerticalScrollBar().setUnitIncrement(10);

		// Button pour ajouer une réponse
		nouvelle = new JButton("Ajouter réponse");
		nouvelle.addActionListener(new ActionBouton_concepteur(fenetre, "ajouter_reponse"));

		// Ajoute des différents radio button
		JRadioButton choix_mul = new JRadioButton("Choix multiples");
		choix_mul.setFont(FontLibrary.fontLabel);
		choix_mul.addItemListener(new RadioBoutonMathieu("choix_mul",fenetre, this));

		choix_libre = new JRadioButton("Choix libre");
		choix_libre.setFont(FontLibrary.fontLabel);
		choix_libre.addItemListener(new RadioBoutonMathieu("choix_libre",fenetre, this));

		JRadioButton choix_unique = new JRadioButton("Choix unique");
		choix_unique.setFont(FontLibrary.fontLabel);
		choix_unique.addItemListener(new RadioBoutonMathieu("choix_unique",fenetre, this));

		JRadioButton choix_class = new JRadioButton("Choix avec classement");
		choix_class.setFont(FontLibrary.fontLabel);
		choix_class.addItemListener(new RadioBoutonMathieu("choix_class",fenetre, this));

		JRadioButton choix_note = new JRadioButton("Notation");
		choix_note.setFont(FontLibrary.fontLabel);
		choix_note.addItemListener(new RadioBoutonMathieu("choix_note",fenetre, this));

		invisible = new JRadioButton();


		// regroupement des radio button
		ButtonGroup choix = new ButtonGroup();


		choix.add(choix_mul);
		choix.add(choix_libre);
		choix.add(choix_unique);
		choix.add(choix_class);
		choix.add(choix_note);
		choix.add(invisible);

		// JPanel des radio Button
		JPanel choix_type = new JPanel();
		choix_type.setLayout(new BoxLayout(choix_type, BoxLayout.Y_AXIS));
		choix_type.setBorder(new LineBorder(Color.BLACK,1));

		// Bouton precedent et suivant
		JButton prec = new JButton(Application.precedent);
		prec.setBackground(new Color(250, 235, 225, 0));
		prec.setPreferredSize(new Dimension(125, 125));
		prec.setContentAreaFilled(false);
		prec.setFocusPainted(false);
		prec.addActionListener(new ActionBouton_concepteur(fenetre, "precedent_question"));

		JButton suiv = new JButton(Application.suivant);
		suiv.setBackground(new Color(250, 235, 225, 0));
		suiv.setPreferredSize(new Dimension(125, 125));
		suiv.setContentAreaFilled(false);
		suiv.setFocusPainted(false);
		suiv.addActionListener(new ActionBouton_concepteur(fenetre, "suivant_question"));


		choix_type.add(choix_mul);
		choix_type.add(choix_libre);
		choix_type.add(choix_unique);
		choix_type.add(choix_class);
		choix_type.add(choix_note);

		// Ajout de la bar de progression
		progression = 1;
		max = 1;
		progressBar = new JProgressBar(0, 1);
		progressBar.setPreferredSize(new Dimension(300, 30));
		progressBar.setValue(progression);
		progressBar.setStringPainted(false);

		legendeProgression = new JLabel("Question : " + progression + "/"  + max);
		droite.add(legendeProgression);
		droite.add(progressBar);

		// Mise en place des JPanel pour donner forme

		// Centre le titre
		posTitre.add(titre);

		// regroupe JLabel et JTextField pour question
		intituler_et_question.add(question);
		intituler_et_question.add(intituler);

		// regroupe titre et question
		fond2.add(posTitre);
		fond2.add(intituler_et_question);

		// ajoute au JPanel principal
		fond.add(fond2);

		// centre le JLabel type de question
		centrer_texte.add(type_question);

		// ajoute les radio button et le JLabel type de question au JPanel border
		border.add(centrer_texte,"North");
		border.add(choix_type,"Center");

		// ajoute le JPanel border et le button pour ajouter une réponse au JPanel choix_et_creation
		choix_et_creation.add(border,"North");
		choix_et_creation.add(nouvelle,"South");

		// Ajoute le JLabel réponse et la list de réponse au JPanel texte_et_reponse
		texte_et_reponse.add(reponse,"North");
		texte_et_reponse.add(scroll,"South");

		// regroupe les deux JPanel choix_et_creation et _texte_et_reponse dans le JPanel contient_choix_reponse
		contient_choix_reponse.add(choix_et_creation);
		contient_choix_reponse.add(texte_et_reponse);

		// Ajoute cotient_choix_reponse au JPanel principal
		fond.add(contient_choix_reponse);

		principal.add(prec,"West");
		principal.add(fond,"Center");
		principal.add(suiv,"East");


		// Ajoute le JPanel principal a la fenetre
		this.add(principal);
		listeReponses.setPreferredSize(new Dimension(550,cpt));
		this.nouvelle.setEnabled(false);

		//Ajout d'une question au questionnaire
		quest = new Question(intituler.getText(), this.type, qu.getidQ(),qu.getNumNextQuestion());
	}

	public JPanel addElem(ValPossible reponse, JFrame fenetre){
		cptRep+=1;
		MiniBoutonSupprimer suppr = new MiniBoutonSupprimer(fenetre, "concepteur");
		suppr.addActionListener(new ActionBouton_concepteur(fenetre, "suppr_question"));
		JPanel crea = new JPanel();
		JPanel plac = new JPanel();
		JLabel reponses = new JLabel(reponse.toString());
		crea.setName(Integer.toString(reponse.getIdValPossible()));
		reponses.setFont(FontLibrary.fontLabel);
		plac.add(reponses);
		crea.setLayout(new BorderLayout());
		crea.add(suppr,"East");
		crea.add(plac,"Center");
		crea.setPreferredSize(new Dimension(475,40));
		crea.setBorder(new LineBorder(Color.BLACK,1));
		cpt+=70;
		crea.setBackground(new Color(255,255,255));
		stock.add(crea);
		return crea;
	}
	
	public void loadNext(){
		if (progression == max){
			progression +=1;
			max += 1;
			legendeProgression.setText("Question : " + progression + "/"  + max);
			this.changeProgressBar();
		} else {
			progression +=1;
			legendeProgression.setText("Question : " + progression + "/"  + max);
			this.changeProgressBar();
		}
		this.fenetre.refresh();
	}

	public void loadPrevious(){
		if (progressBar.getValue() != 0){
			progression -=1;
			legendeProgression.setText("Question : " + progression + "/"  + max);
			this.changeProgressBar();
		}
		this.fenetre.refresh();
	}

	public void changeProgressBar(){
		droite.remove(progressBar);
		progressBar = new JProgressBar(0,max);
		progressBar.setValue(progression);
		progressBar.setPreferredSize(new Dimension(300, 30));
		progressBar.setStringPainted(false);
		droite.add(progressBar);
	}
}
