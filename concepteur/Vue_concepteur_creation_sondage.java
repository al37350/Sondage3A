package concepteur;

import Modele.*;
import base.*;
import controleur.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;
import java.util.ArrayList;
import Modele.*;

public class Vue_concepteur_creation_sondage extends Vue_globale{
	public ArrayList<Client> clients;
	public ArrayList<PanelSonde> panels;
	public static JTextField nomSondage;
	public JPanel clientSelected;
	public JPanel panelSelected;
	public ArrayList<JPanel> listePanelPanel;
	public ArrayList<JPanel> listePanelClient;
	public static int idClientSelect;
	public static int idPanelSelect;
	public JLabel error;

	public Vue_concepteur_creation_sondage(JFrame fenetre){
		super(fenetre, "annuler", "basic", "basic", "concepteur");

		listePanelClient = new ArrayList<JPanel>();
		listePanelPanel = new ArrayList<JPanel>();

		panels = PanelSonde.getAllPanel(Application.connection);
		clients = RqtConcepteur.allClient(Application.connection);


		
		// Création des panel et des layout
		JPanel princ = new JPanel();
		princ.setLayout(new FlowLayout());

		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.CENTER);
		layout.setVgap(30);

		JPanel titre = new JPanel();
		titre.setLayout(layout);

		JPanel nomSond = new JPanel();
		titre.setLayout(layout);

		FlowLayout layout2 = new FlowLayout();
		layout2.setAlignment(FlowLayout.CENTER);
		layout2.setHgap(300);
		layout2.setVgap(20);

		FlowLayout layout3 = new FlowLayout();
		layout3.setAlignment(FlowLayout.CENTER);
		layout3.setVgap(40); 
		layout3.setHgap(50);

		FlowLayout layout4 = new FlowLayout();		
		layout4.setVgap(5); 
		layout4.setAlignment(FlowLayout.LEFT);

		FlowLayout layout5 = new FlowLayout();				
		layout5.setAlignment(FlowLayout.LEADING);

		FlowLayout layout6 = new FlowLayout();
		layout6.setAlignment(FlowLayout.CENTER);
		layout6.setVgap(25); 


		JPanel lierClientPrinc = new JPanel();
		lierClientPrinc.setLayout(new BorderLayout());
		lierClientPrinc.setBackground(ColorLibrary.beige);

		JPanel lierClient = new JPanel();		
		lierClient.setLayout(layout6);					
		lierClient.setPreferredSize(new Dimension(360,68*(clients.size()+1)));
		lierClient.setBackground(ColorLibrary.beige);


		JScrollPane jspClient = new JScrollPane();
		jspClient.setViewportView(lierClient);		
		jspClient.setBorder(new LineBorder(Color.black,2));
		jspClient.setPreferredSize(new Dimension(390,225));
		jspClient.getVerticalScrollBar().setUnitIncrement(10);
		jspClient.setBackground(ColorLibrary.beige);
		jspClient.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));

		JLabel labelClient = new JLabel("Lier client :");
		labelClient.setFont(FontLibrary.fontLabel);
		labelClient.setBackground(ColorLibrary.beige);

		JPanel panelLabelClient = new JPanel();
		panelLabelClient.setLayout(layout4);
		panelLabelClient.add(labelClient);
		panelLabelClient.setBackground(ColorLibrary.beige);		

		JPanel lierPanelPrinc = new JPanel();
		lierPanelPrinc.setLayout(new BorderLayout());
		lierPanelPrinc.setBackground(ColorLibrary.beige);

		JPanel lierPanel = new JPanel();
		lierPanel.setLayout(layout6);			
		lierPanel.setPreferredSize(new Dimension(360,68*(panels.size()+1)));
		lierPanel.setBackground(ColorLibrary.beige);

		JLabel labelPanel = new JLabel("Lier panel :");
		labelPanel.setFont(FontLibrary.fontLabel);
		labelPanel.setBackground(ColorLibrary.beige);

		JPanel panelLabelPanel = new JPanel();
		panelLabelPanel.setLayout(layout4);
		panelLabelPanel.add(labelPanel);
		panelLabelPanel.setBackground(ColorLibrary.beige);	
		

		JScrollPane jspPanel = new JScrollPane();
		jspPanel.setViewportView(lierPanel);
		jspPanel.setBorder(new LineBorder(Color.black,2));			
		jspPanel.setPreferredSize(new Dimension(390,225));
		jspPanel.getVerticalScrollBar().setUnitIncrement(10);
		jspPanel.setBackground(ColorLibrary.beige);
		jspPanel.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));

		ActionCreationSondage acs = new ActionCreationSondage(fenetre, this);
		JPanel client;
		JPanel bouton;
		JPanel labClient;		
		JLabel nom;
		JButton lier;

		GridLayout grid = new GridLayout(1,2);		
		grid.setHgap(40); 

		FlowLayout layout7 = new FlowLayout();		
		layout7.setHgap(40); 
		layout7.setVgap(12); 
		


		for(int i=0; i<clients.size();i++){
			client = new JPanel();
			client.setLayout(grid);
			client.setPreferredSize(new Dimension(340,50));
			client.setBorder(new LineBorder(Color.black,1));
			client.setBackground(Color.WHITE);	

			nom = new JLabel(clients.get(i).getRaisonSociale());
			nom.setFont(FontLibrary.fontLabel);

			labClient = new JPanel();
			labClient.setLayout(layout7);
			labClient.setBackground(new Color(0,0,0,0));	
			labClient.add(nom);

			
			client.add(labClient);

			lier = new JButton("Lier");					
			lier.addActionListener(acs);
			lier.setName("lierClient"+Integer.toString(i));
			lier.setBackground(new Color(220,220,220,50));		
			lier.setBorder(new LineBorder(Color.black,1));
			lier.setPreferredSize(new Dimension(120,39));
			lier.setFont(FontLibrary.fontLabel);

			bouton = new JPanel();
			bouton.setLayout(new FlowLayout());
			bouton.setBackground(new Color(0,0,0,0));

			bouton.add(lier);

			client.add(bouton);

			lierClient.add(client);
			listePanelClient.add(client);
		}
		

		JPanel panel;		
		JPanel labPanel;		
		


		for(int i=0; i<panels.size();i++){
			panel = new JPanel();
			panel.setLayout(grid);
			panel.setPreferredSize(new Dimension(340,50));
			panel.setBorder(new LineBorder(Color.black,1));
			panel.setBackground(Color.WHITE);	

			nom = new JLabel(panels.get(i).getName());
			nom.setFont(FontLibrary.fontLabel);

			labPanel = new JPanel();
			labPanel.setLayout(layout7);
			labPanel.setBackground(new Color(0,0,0,0));	
			labPanel.add(nom);

			
			panel.add(labPanel);

			lier = new JButton("Lier");					
			lier.addActionListener(acs);
			lier.setName("lierPanel"+Integer.toString(i));
			lier.setBackground(new Color(220,220,220,50));		
			lier.setBorder(new LineBorder(Color.black,1));
			lier.setPreferredSize(new Dimension(120,39));
			lier.setFont(FontLibrary.fontLabel);

			bouton = new JPanel();
			bouton.setLayout(new FlowLayout());
			bouton.setBackground(new Color(0,0,0,0));

			bouton.add(lier);

			panel.add(bouton);

			lierPanel.add(panel);
			listePanelPanel.add(panel);
		}

		JPanel lierPrinc = new JPanel();
		lierPrinc.setLayout(layout3);
		lierPrinc.setBackground(ColorLibrary.beige);

		JLabel labelParam = new JLabel("<html>Paramètres du<br>sondage :</html>");
		labelParam.setFont(FontLibrary.fontLabel);
		labelParam.setPreferredSize(new Dimension(150,150));
		

		JPanel panelParam = new JPanel();
		panelParam.setLayout(layout5);
		panelParam.add(labelParam);
		panelParam.setBackground(ColorLibrary.beige);

		JButton boutonClient = new JButton("Créer client");
		boutonClient.setName("creerClient");
		boutonClient.addActionListener(acs);
		boutonClient.setFont(FontLibrary.fontLabel);
		boutonClient.setBackground(Color.WHITE);
		boutonClient.setBorder(new LineBorder(Color.black,2));
		boutonClient.setPreferredSize(new Dimension(150,45));


		JPanel bClient = new JPanel();
		bClient.setPreferredSize(new Dimension(420,100));
		bClient.setLayout(layout4);
		bClient.setBackground(ColorLibrary.beige);

		bClient.add(boutonClient);
 

		lierClientPrinc.add(panelLabelClient,"North");
		lierClientPrinc.add(jspClient,"Center");
		lierPanelPrinc.add(panelLabelPanel,"North");
		lierPanelPrinc.add(jspPanel,"Center");

		lierPrinc.add(panelParam);
		lierPrinc.add(lierClientPrinc);
		lierPrinc.add(lierPanelPrinc);
		lierPrinc.setBackground(ColorLibrary.beige);



		// Création du titre
		JLabel labelTitre = new JLabel("Création d'un nouveau questionnaire");
		labelTitre.setFont(FontLibrary.fontTitre);

		// Création de la partie nommage du sondage
		JLabel labelNomSond = new JLabel("Nom du sondage :");
		labelNomSond.setFont(FontLibrary.fontLabel);
		nomSondage = new JTextField(35);
		nomSondage.setFont(FontLibrary.fontTextField);
		nomSondage.setDocument(new JTextLimited(30));

		// Création bouton validation
		JButton valider = new JButton(Application.valider);
		valider.setName("valider");
		valider.addActionListener(acs);
		valider.setPreferredSize(new Dimension(100,0));		
		valider.setBorder(new LineBorder(Color.black,1));
		valider.setBackground(ColorLibrary.beige);
		this.add(valider,"East");

		// Création label erreur :
		error = new JLabel();
		error.setFont(FontLibrary.fontErreur);
		error.setForeground(Color.RED);

		// Ajouts :
		//Ajout titre 
		titre.add(labelTitre);
		titre.setBackground(ColorLibrary.beige);

		//Ajouts partie nommage
		nomSond.add(labelNomSond);
		nomSond.add(nomSondage);
		nomSond.setBackground(ColorLibrary.beige);

		// Ajouts panel principal
		princ.add(titre);	
		princ.add(nomSond);			
		princ.add(lierPrinc);
		princ.add(bClient);
		princ.add(error);

		princ.setBackground(ColorLibrary.beige);
		

		this.add(princ,"Center");





	}
}