
package concepteur;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import Modele.*;
import java.util.ArrayList;

import Modele.*;
import base.*;
import controleur.*;

public class Vue_concepteur_modifierClient extends Vue_globale {

	private JButton selected;
	private ArrayList<Client> listeCli = new ArrayList<Client>();
	public static Client cli;

		public Vue_concepteur_modifierClient(JFrame fenetre){
			super(fenetre, "annuler", "basic", "basic", "concepteur");

			listeCli = RqtConcepteur.allClient(Application.connection);

			
			// Création des panel et des layout
			JPanel princ = new JPanel();
			princ.setLayout(new FlowLayout());
			princ.setBackground(new Color(240, 235, 225, 100));


			FlowLayout layout = new FlowLayout();
			layout.setAlignment(FlowLayout.CENTER);
			layout.setVgap(40);

			FlowLayout layout2 = new FlowLayout();
			layout2.setAlignment(FlowLayout.CENTER);
			layout2.setHgap(300);
			layout2.setVgap(20);
			

			JPanel titre = new JPanel();
			titre.setLayout(layout);
			titre.setBackground(new Color(240, 235, 225, 100));

			JPanel quest = new JPanel();
			quest.setLayout(layout2);
			quest.setBackground(new Color(240, 235, 225, 100));

			// Définition des polices
			Font fontTitre = new Font("Liberation Sans", Font.BOLD, 38);
			Font fontLabel = new Font("Calibri",0,18);
			Font fontTextField = new Font("Calibri",0,25);

			// Création du titre
			JLabel labelTitre = new JLabel("Sélectionnez un client à modifier");
			labelTitre.setFont(fontTitre);
			titre.add(labelTitre);

			// Création du label questionnaire
			JLabel labelQuest = new JLabel("Client :");
			labelQuest.setFont(fontLabel);
			quest.add(labelQuest);

			// Création du panel de choix d'un sondage à analyser
			FlowLayout layout3 = new FlowLayout();
			layout3.setAlignment(FlowLayout.CENTER);
			layout3.setVgap(25); 

			JPanel choix = new JPanel();
			choix.setLayout(layout3);			
			choix.setBorder(new LineBorder(Color.black,2));		
			choix.setPreferredSize(new Dimension(1000,65*(listeCli.size()+1)));
			choix.setBackground(new Color(240, 235, 225, 100));

					
			// Test test test

			//ActionChoix ac = new ActionChoix(fenetre,this);

			

			for(int i=0;i<listeCli.size();i++){
				JPanel elem = this.addElem(listeCli.get(i),""+(i+1),fenetre);
				
				elem.setPreferredSize(new Dimension(900,40));
				elem.setBackground(Color.WHITE);
				choix.add(elem);
			}

			

			//boutonAnalyse.addActionListener(ac);

			
			JScrollPane jsp = new JScrollPane();
			jsp.setViewportView(choix);		
			jsp.setPreferredSize(new Dimension(1020,300));
			jsp.getVerticalScrollBar().setUnitIncrement(10);
			jsp.setBackground(new Color(240, 235, 225, 100));
			jsp.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));


			JPanel choixQuest = new JPanel();
			choixQuest.setLayout(new BorderLayout());
			choixQuest.add(labelQuest,"North");
			choixQuest.add(jsp,"Center");
			choixQuest.setBackground(new Color(240, 235, 225, 100));

			FlowLayout layout4 = new FlowLayout(); 		
			layout4.setVgap(35); 
			layout4.setAlignment(FlowLayout.RIGHT);

			JPanel bAnalyse = new JPanel();
			bAnalyse.setPreferredSize(new Dimension(750,200));
			bAnalyse.setLayout(layout4);
			bAnalyse.setBackground(new Color(240, 235, 225, 100));

			princ.add(titre);	
			princ.add(choixQuest);
			princ.add(bAnalyse);
			
			selected=null;
			

			this.add(princ,"Center");
		}

		public JButton getSelected(){
			return this.selected;
		}

		public void setSelected(JButton s){
			this.selected=s;
		}

		public Client getClient(int indice){
			return listeCli.get(indice);
		}
		public JPanel addElem(Client nomClient, String numSondage,JFrame fen){
			MiniBoutonSupprimer suppr = new MiniBoutonSupprimer(fenetre, "supprimer_client");
			suppr.addActionListener(new Action_Bouton_ClientEdit(fenetre, "supprimer_client"));
			suppr.setName(""+nomClient.getNumero());
			
			MiniBoutonEditer editer = new MiniBoutonEditer(fenetre, "editer_client");
			editer.addActionListener(new Action_Bouton_ClientEdit(fenetre, "edit_client"));
			editer.setName(""+nomClient.getNumero());

			JPanel bouton = new JPanel();
			bouton.setBackground(Color.WHITE);
			bouton.add(editer);
			bouton.add(suppr);
			JPanel crea = new JPanel();
			JLabel client = new JLabel(nomClient.getRaisonSociale());
			client.setFont(new Font("Calibri",0,22));
			JPanel placer_txt = new JPanel();
			placer_txt.setBackground(Color.WHITE);			
			placer_txt.add(client);
			crea.setLayout(new BorderLayout());			
			crea.add(placer_txt,"Center");
			crea.add(bouton,"East");
			crea.setBackground(Color.WHITE);
			crea.setPreferredSize(new Dimension(650,40));
			crea.setBorder(new LineBorder(Color.BLACK,1));
			return crea;
			
		}
	}