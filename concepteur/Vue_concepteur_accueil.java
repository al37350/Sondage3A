package concepteur;

import Modele.*;
import base.*;
import controleur.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class Vue_concepteur_accueil extends Vue_globale {

	public static Vue_concepteur_creation_sondage vcs;
	
		public Vue_concepteur_accueil(JFrame fenetre){

		super(fenetre, "concepteur");

		//Les JPanel
		FlowLayout flowtitre = new FlowLayout();
		flowtitre.setVgap(50);

		JPanel titre = new JPanel();
		titre.setLayout(flowtitre);

		JPanel page = new JPanel();
		page.setLayout(new BorderLayout());

		JPanel ensBoutons = new JPanel();
		ensBoutons.setLayout(new FlowLayout());

		JPanel ensBoutons1 = new JPanel();
		ensBoutons1.setLayout(new FlowLayout());

		JPanel ensBoutons2 = new JPanel();
		ensBoutons2.setLayout(new FlowLayout());

		FlowLayout qqch = new FlowLayout();
		qqch.setHgap(60);
		qqch.setVgap(40);

		JPanel bouton1 = new JPanel();
		bouton1.setLayout(qqch);

		JPanel bouton2 = new JPanel();
		bouton2.setLayout(qqch);

		JPanel bouton3 = new JPanel();
		bouton3.setLayout(qqch);

		JPanel bouton4 = new JPanel();
		bouton4.setLayout(qqch);


		//intitulé de la page
		JLabel accConcept = new JLabel("Accueil concepteur");		
		accConcept.setFont(FontLibrary.fontTitre);
		
		
		//actionlistneur
		
		
		//Les JButton
		JButton b1 = new JButton("Ajouter client");
		JButton b2 = new JButton("Créer sondage");
		JButton b3 = new JButton("Éditer clientèle");
		JButton b4 = new JButton("Éditer sondage");

		b1.setPreferredSize(new Dimension(300,100));
		b2.setPreferredSize(new Dimension(300,100));
		b3.setPreferredSize(new Dimension(300,100));
		b4.setPreferredSize(new Dimension(300,100));

		b1.setBackground(Color.WHITE);
		b1.setBorder(new LineBorder(Color.black,1));
		b1.setFont(FontLibrary.fontBouton);
		b2.setBackground(Color.WHITE);
		b2.setBorder(new LineBorder(Color.black,1));
		b2.setFont(FontLibrary.fontBouton);
		b3.setBackground(Color.WHITE);
		b3.setBorder(new LineBorder(Color.black,1));
		b3.setFont(FontLibrary.fontBouton);
		b4.setBackground(Color.WHITE);
		b4.setBorder(new LineBorder(Color.black,1));
		b4.setFont(FontLibrary.fontBouton);

		b1.setName("AjoutClient");
		b2.setName("CreationSondage");
		b3.setName("EditClientel");
		b4.setName("ModificationSondage");
		
		b1.addActionListener(new ActionBouton_concepteur(fenetre, "ajout_client"));
		b2.addActionListener(new ActionBouton_concepteur(fenetre, "creation_sondage"));
		b3.addActionListener(new ActionBouton_concepteur(fenetre, "edition_client"));
		b4.addActionListener(new ActionBouton_concepteur(fenetre, "modif_sondage"));
		
		//Ajout des JPanel
		titre.add(accConcept);

		bouton1.add(b1);
		bouton2.add(b2);
		bouton3.add(b3);
		bouton4.add(b4);

		ensBoutons1.add(bouton1);
		ensBoutons1.add(bouton2);
		ensBoutons2.add(bouton3);
		ensBoutons2.add(bouton4);

		ensBoutons.add(ensBoutons1);
		ensBoutons.add(ensBoutons2);

		page.add(titre, BorderLayout.NORTH);
		page.add(ensBoutons,BorderLayout.CENTER);
		page.setBackground(ColorLibrary.beige);


		this.add(page, BorderLayout.CENTER);
	}	
}