package Modele;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.*;

public class RqtAnalyste extends RqtCommunes{

	public static ArrayList<Questionnaire> getAllQuestionnaire(ConnectionMySql connection){
		ArrayList<Questionnaire> liste = new ArrayList<Questionnaire>();
		ResultSet result = connection.request("select * from QUESTIONNAIRE where Etat='A'");
		try{
			while(result.next()){
				liste.add(new Questionnaire(result.getInt("idQ"),result.getString("Titre"),result.getString("Etat"),result.getInt("idPan"), RqtCommunes.getAllQuestion(result.getInt("idQ"), connection)));
			}
		}
		catch(SQLException e){
			return liste;
		}
		return liste;
	}
}