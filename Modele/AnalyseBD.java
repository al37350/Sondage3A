package Modele;
import java.sql.*;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.ArrayList;
import javax.swing.*;
import java.util.Collections;

public class AnalyseBD{
    private class Couple{
		private String premier;
		private Integer second;
		private Couple(String prem, Integer sec){
			this.premier = prem;
			this.second = sec;
		}
		private String getFirst(){
			return this.premier;
		}
		private Integer getSecond(){
			return this.second;
		}
    public String toString(){
    	return "("+premier+", "+second+")";
    }
    }
    private ConnectionMySql connection;
    public AnalyseBD(ConnectionMySql connection){
		this.connection = connection;
    }
    public  HashMap<String,ArrayList<Couple>> analyseAge(int numQuestion, int numQuestionnaire){
		HashMap<String,ArrayList<Couple>> listeRep = new HashMap<String,ArrayList<Couple>>();
		ArrayList<Couple> listeVal = new ArrayList<Couple>();
		String rep="";
		Integer compte = new Integer(0);
		ResultSet result=connection.request("select idC,valeur,count(valeur) from REPONDRE where idQ="+numQuestionnaire+" and numQ="+numQuestion+" group by idC,valeur order by valeur,idC;");
		try{
			while(result.next()){
				if(!result.getString("valeur").equals(rep)){
					if(!rep.equals("")){
						listeRep.put(rep,listeVal);
					}
					rep=result.getString("valeur");
					listeVal = new ArrayList<Couple>();
				}
				listeVal.add(new Couple(result.getString("idC").substring(1),result.getInt("count(valeur)")));
			}
			listeRep.put(rep,listeVal);
		}
		catch(SQLException e){
		}
		return listeRep;
		
    }
    public JTable analyseCat(int numQuestion, int numQuestionnaire){
		TreeMap<String,ArrayList<Couple>> listeRep = new TreeMap<String,ArrayList<Couple>>();
		ArrayList<Couple> listeVal = new ArrayList<Couple>();
		String rep="";
		ResultSet result=connection.request("select idQ,numQ,intituleCat,valeur,count(valeur) from REPONDRE natural join 	CARACTERISTIQUE natural join CATEGORIE where idQ="+numQuestionnaire+" and numQ="+numQuestion+" group by idCat,valeur order by valeur,idCat;");
		try{
			while(result.next()){
				if(!result.getString("valeur").equals(rep)){
					if(!rep.equals("")){
						try{
							Integer.parseInt(rep);
							if(rep.length()>1)
								listeRep.put(rep,listeVal);
							else
								listeRep.put("0"+rep,listeVal);
						}
						catch(Exception e){
							listeRep.put(rep,listeVal);
						}
						
					}
					rep=result.getString("valeur");
					listeVal = new ArrayList<Couple>();
				}
					listeVal.add(new Couple(result.getString("intituleCat"),result.getInt("count(valeur)")));
			}
			try{
				Integer.parseInt(rep);
				if(rep.length()>1)
					listeRep.put(rep,listeVal);
				else
					listeRep.put("0"+rep,listeVal);
			}
			catch(Exception e){
				listeRep.put(rep,listeVal);
			}
		}
		catch(SQLException e){
		}
		Object[][] donnes = new Object[listeRep.size()+1][9];
		String[] entetes = {"Réponses", "Agriculteurs exploitants", "Artisans, commerçants, chefs d", "Cadres, professions intellectu", "Professions intermédiaires", "Employés", "Ouvriers", "Inactifs ayant déjà travaillé", "Autres sans activité professio"};
	    donnes[0] = entetes;
	    int j = 1;
	    for(String mapKey : listeRep.keySet()) {
			ArrayList<Couple> valeur = listeRep.get(mapKey);
	    	int i = 0;
	    	int y = 1;
			String[] tab = new String[9];
			tab[0] ="<html><center>"+ mapKey +"</center></html>";
			while(i < valeur.size()){
				if(entetes[y].equals(valeur.get(i).getFirst())){
					tab[y]="<html><center>"+Integer.toString(valeur.get(i).getSecond())+"</center></html>";
					i++;
					y++;
				}
				else{
					tab[y]="<html><center>0</center></html>";
					y++;

				}
				
			}
			donnes[j] = tab;
			j+=1;
	    }
	    
	     JTable tableau = new JTable(donnes,entetes) {
        private static final long serialVersionUID = 1L;

        public boolean isCellEditable(int row, int column) {                
                return false;               
        };
    };
	    tableau.setRowHeight(400/listeRep.size()+1);
	    return tableau;
    }
}

