package Modele;
import java.util.ArrayList;
import java.sql.*;

public class PanelSonde {
	private int id;
	private String nom;

	public PanelSonde(int id, String nom){
		this.id = id;
		this.nom = nom;
	}

	public int getId(){
		return this.id;
	}

	public String getName(){
		return this.nom;
	}

	public void setId(int id){
		this.id = id;
	}

	public void setName(String nom){
		this.nom = nom;
	}

	public static ArrayList<PanelSonde> getAllPanel(ConnectionMySql connection){
		ArrayList<PanelSonde> c = new ArrayList<PanelSonde>();
		ResultSet result = connection.request("select * from PANEL");
		try{
			while(result.next()){
				c.add(new PanelSonde(result.getInt("idPan"),result.getString("nomPan")));
			}
			return c;		
		} catch (Exception e){
			return null;
		}
	}
}