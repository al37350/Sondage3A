package Modele;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.*;

public abstract class RqtCommunes{

	public static Utilisateur connectUser(String id, String password, ConnectionMySql connection){
		ResultSet result = connection.request("select * from UTILISATEUR where login = '" + id + "';");
		try{
			result.next();
			if (result.getString("motDePasse").equals(password)){
				return new Utilisateur(result.getInt("idU"), result.getString("nomU"), result.getString("prenomU"), id, result.getInt("idR"));
			} else {
				return null;
			}
		} catch (Exception e){
			return null;
		}
	}

	public void disconnect(ConnectionMySql connection){
		connection.close();
	}


	public static Questionnaire getQuestionnaire(int numero, ConnectionMySql connection){
		ResultSet result = connection.request("select * from QUESTIONNAIRE where idQ = " + numero);
		try{
			result.next();
			return new Questionnaire(result.getInt("idQ"),result.getString("Titre"),result.getString("Etat"),result.getInt("idPan"), RqtCommunes.getAllQuestion(numero, connection), RqtConcepteur.findClient(result.getInt("numC"), connection));
		}
		catch(SQLException e){
			return null;
		}

	}
	
	public static Question getQuestion(int idQuestionnaire, int numQuestion, ConnectionMySql connection){
		ResultSet result = connection.request("select * from QUESTION where idQ = " + idQuestionnaire + " and numQ="+numQuestion);
		try{
			result.next();
			return new Question(result.getString("texteQ"), result.getString("idT").charAt(0), result.getInt("idQ"), result.getInt("numQ"));
		
		} catch (Exception e){
			return null;
		}
	}

	public static ArrayList<Question> getAllQuestion(int idQuestionnaire, ConnectionMySql connection){
		ArrayList<Question> questions = new ArrayList<Question>();
		ResultSet result = connection.request("select * from QUESTION where idQ = " + idQuestionnaire);
		try{
			while(result.next()){
				questions.add(new Question(result.getString("texteQ"), result.getString("idT").charAt(0), result.getInt("idQ"), result.getInt("numQ"), RqtCommunes.getAllProposition(result.getInt("idQ"), result.getInt("numQ"), connection)));
			}
			return questions;		
		} catch (Exception e){
			return null;
		}
	}

	public static String getProposition(int idQuestionnaire, int numQuestion, int idValPossible, ConnectionMySql connection){
		ResultSet result = connection.request("select Valeur from VALPOSSIBLE where idQ = " + idQuestionnaire + " and numQ="+numQuestion+" and idV="+idValPossible);
		try{
			result.next();
			return result.getString("Valeur");
		
		} catch (Exception e){
			return null;
		}
	}

	public static ArrayList<ValPossible> getAllProposition(int idQuestionnaire, int numQuestion, ConnectionMySql connection){
		ArrayList<ValPossible> listeVal=new ArrayList<ValPossible>();
		ResultSet result = connection.request("select * from VALPOSSIBLE where idQ = " + idQuestionnaire + " and numQ="+numQuestion);
		try{
			while(result.next()){
				listeVal.add(new ValPossible(idQuestionnaire, numQuestion, result.getInt("idV"), result.getString("Valeur")));
			}
			return listeVal;		
		} catch (Exception e){
			return null;
		}
	}

	public static void setProposition(ValPossible v, ConnectionMySql connection){
		Object [] elements = {v.getIdQuestionnaire(), v.getNumQuestion(), v.getIdValPossible(), v.getTexte()};
		String [] types = {"int", "int", "int", "String"};
		//System.out.println(connection);
		// try{
			connection.update("insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values (?, ?, ?, ?)", elements, types);
		// }
		// catch(MySQLIntegrityConstraintViolationException e){
		// 	System.out.println("Il y à déjà une réponse avec cette id");
		//  }
	}

	public static void deleteProposition(int idQuestionnaire, int idQuestion, int idReponse, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire,idQuestion, idReponse};
		String [] types = {"int","int","int"};
		connection.update("delete from VALPOSSIBLE where idQ = ? and numQ=? and idV= ?", elements, types);
	}
}


// pas de update questionnaire ?
//updates ne marchent pas