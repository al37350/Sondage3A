package Modele;
import java.sql.*;

public class ConnectionMySql{
	Connection connection;
	boolean status;
	public ConnectionMySql(){
		this.connect();
	}


	/* 
	* Make the connection to a database
	*/
	public void connect(){
		System.out.println("-------- Test connexion MySQL ------------");
	 
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Driver MySQL non trouvé.");
			return;
		}
		System.out.println("Driver MySQL trouvé");

		
		connection = null;
		String login = "dblamirault";
		String pass = "lamirault";
		try {
			connection = DriverManager.getConnection("jdbc:mysql://servinfo-db:3306/"+login, login, pass);
		} catch (SQLException e) {
			System.out.println("Message : "+e.getMessage()+e.getErrorCode());
			return;
		}
		System.out.println("connexion réussie");
		this.status = true;
	}

	public boolean status(){
		return this.status;
	}


	public ResultSet request(String requete){
		try{
			PreparedStatement statement = this.connection.prepareStatement(requete);
			ResultSet answer = statement.executeQuery();
			return answer;
		} catch (Exception e){
			System.out.println("Echec de la requete.\n"+ e);
			return null;
		}
	}

	public void update(String requete, Object [] elements, String [] types){
		try{
			PreparedStatement statement = this.connection.prepareStatement(requete);
			for (int i = 0; i < elements.length; i++){
				switch (types[i]){
					case "int": statement.setInt(i+1, (int) elements[i]); break;
					case "String": statement.setString(i+1, (String) elements[i]); break;
				}
			}
			statement.executeUpdate();
		} catch (Exception e){
			System.out.println("Echec de la requete.\n"+ e);
		}
	}



	public void close(){
		try {
			connection.close();
			System.out.println("déconnexion réussie");
		} catch (SQLException e) {
			System.out.println("pb de déconnection");
		}
		this.status = false;
	}
}