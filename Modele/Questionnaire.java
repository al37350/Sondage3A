package Modele;
import java.util.ArrayList;

public class Questionnaire{
    private int idQ;
    private String titre;
    private String etat;
    private int idPan;
    private Client cli;
    private ArrayList<Question> listeQ;

    public Questionnaire(int idQ, String titre, String etat, int idPan, Client cli){
    	this.idQ = idQ;
    	this.titre = titre;
    	this.etat = etat;
    	this.idPan = idPan;
    	this.cli = cli;
    	this.listeQ = new ArrayList<Question>();
    }

    public Questionnaire(int idQ, String titre, String etat, int idPan){
    	this.idQ = idQ;
    	this.titre = titre;
    	this.etat = etat;
    	this.idPan = idPan;
    	this.listeQ = new ArrayList<Question>();
    }

    public Questionnaire(int idQ, String titre, String etat, int idPan, ArrayList<Question> liste){
        this.idQ = idQ;
        this.titre = titre;
        this.etat = etat;
        this.idPan = idPan;
        this.listeQ = liste;
    }

    public Questionnaire(int idQ, String titre, String etat, int idPan, ArrayList<Question> liste, Client cli){
    	this.idQ = idQ;
    	this.titre = titre;
    	this.etat = etat;
    	this.cli = cli;
    	this.idPan = idPan;
    	this.listeQ = liste;
    }
    
    public int getNumNextQuestion(){
        if (listeQ.size()==0){
            return 1;
        }
        int j=listeQ.get(0).getNumQuestion();
        for(int i=0; i<listeQ.size(); i++){
            if (listeQ.get(i).getNumQuestion()>j){
                j=listeQ.get(i).getNumQuestion();
            }
        }
        return j+1;
    }

    public int getidQ(){
	   return this.idQ;
    }

    public String getTitre(){
	   return this.titre;
    }

    public String getEtat(){
	   return this.etat;
    }

    public int getIdPan(){
	   return this.idPan;
    }

    public Question getQuestion(int indice){
	   return this.listeQ.get(indice);
    }

    public ArrayList<Question> getAllQuestion(){
        return this.listeQ;
    }


    public int getNbQuestions(){
        return this.listeQ.size();
    }

    public void addQuestion(Question q){
	   listeQ.add(q);
    }
    
    public void setListeQuestion(ArrayList<Question> liste){
    	listeQ = liste;
    }

    public Client getCli(){
	   return this.cli;
    }

    public void setidQ(int id){
	   this.idQ=id;
    }

    public void setTitre(String titre){
	   this.titre=titre;
    }

    public void setEtat(String etat){
	   this.etat=etat;
    }

    public void setIdPan(int id){
	   this.idPan=id;
    }

    public void setCli(Client cli){
	   this.cli = cli;
    }

    public String toString(){
        return this.titre;
    }

    public boolean contient(String s){
        for (Question q : listeQ) {
            if(q.toString().equals(s)){
                return true;
            }
        }
        return false;
    }
}
