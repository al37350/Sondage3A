package Modele;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.*;

public class RqtConcepteur extends RqtCommunes{
	
	public static int maxNumClient(ConnectionMySql connection){
		ResultSet r=connection.request("select max(NumC) from CLIENT");
		try{
			if (r.next()) {
			return r.getInt(1);
			}
			return -1;
		}
		catch(Exception e){
			return -1;
		}
	}

	public static Client findClient(int numC, ConnectionMySql connection){
		ResultSet result = connection.request("select * from CLIENT where numC="+numC);
		try{
			result.next();
			return new Client(result.getInt("numC"), result.getString("raisonSoc"), result.getString("adresse1"), result.getString("adresse2"), result.getInt("CodePostal"), result.getString("Ville"), result.getString("Telephone"), result.getString("email"));
		}
		catch(Exception e){
			return null;
		}
	}

	public static ArrayList<Client> allClient(ConnectionMySql connection){
		ArrayList<Client> c = new ArrayList<Client>();
		ResultSet result = connection.request("select * from CLIENT");
		try{
			while(result.next()){
				c.add(new Client(result.getInt("numC"), result.getString("raisonSoc"), result.getString("adresse1"), result.getString("adresse2"), result.getInt("CodePostal"), result.getString("Ville"), result.getString("Telephone"), result.getString("email")));
			}
			return c;		
		} catch (Exception e){
			return null;
		}
	}

	public static void addClient(Client c,ConnectionMySql connection){
		Object [] elements = {c.getNumero(), c.getRaisonSociale(), c.getAdresse1(), c.getAdresse2(), c.getCodePostal(), c.getVille(), c.getTelephone(), c.getEmail()};
		String [] types = {"int", "String", "String", "String", "int", "String", "String", "String"};
		connection.update("insert into CLIENT(numC, raisonSoc, adresse1, adresse2, CodePostal, Ville, Telephone, email) values (?, ?, ?, ?, ?, ?, ?, ?);", elements, types);
	}

	public static void deleteClient(int numCli, ConnectionMySql connection){
		ResultSet result = connection.request("select idQ from QUESTIONNAIRE where numC="+numCli);
		Object [] elements= new Object[1];
		String [] types = {"int"};
		try{
			while(result.next()){
				elements[0] = result.getInt("idQ");			
				RqtConcepteur.deleteQuestionnaire((int)elements[0], connection);
			}
			elements[0] = numCli;			
			connection.update("delete from CLIENT where numC =?", elements, types);
		}
		catch(Exception e){
			System.out.println(e);
		}
	}

	public static void updateClient(Client c, ConnectionMySql connection){
		int numero = c.getNumero();
		Object [] elements = {c.getRaisonSociale(), c.getAdresse1(), c.getAdresse2(), c.getCodePostal(), c.getVille(), c.getTelephone(), c.getEmail()};
		String [] types = {"String", "String", "String", "int", "String", "String", "String"};
		connection.update("UPDATE CLIENT SET raisonSoc=?, adresse1=?, adresse2=?, CodePostal=?, Ville=?, Telephone=?, email=? WHERE numc="+c.getNumero(),elements,types);
	}


	public static void setQuestionnaire(Questionnaire q, Client c, Utilisateur u, ConnectionMySql connection){
		Object [] elements = {q.getidQ(), q.getTitre(), q.getEtat(), c.getNumero(), u.getId(), q.getIdPan()};
		String [] types = {"int", "String", "String", "int", "int", "int"};
		connection.update("insert into QUESTIONNAIRE(idQ, Titre, Etat, numC, idU, idPan) values (?, ?, ?, ?, ?, ?)", elements, types);
		try{
			for(Question question: q.getAllQuestion()){
				RqtConcepteur.setQuestion(question,connection);
				for(ValPossible v: question.getPropositions()){
					RqtConcepteur.setProposition(v, connection);
				}
			}
		} catch (Exception exp){
			// System.out.println("");
		}
	}

	public static void deleteQuestionnaire(int idQuestionnaire, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire};
		String [] types = {"int"};
		RqtConcepteur.deleteQuestions(idQuestionnaire, connection);
		connection.update("delete from INTERROGER where idQ = ?", elements, types);
		connection.update("delete from REPONDRE where idQ = ?", elements, types);
		connection.update("delete from QUESTIONNAIRE where idQ = ?", elements, types);
	}

	public static int getNumNextQuestionnaire(ConnectionMySql connection){
		ResultSet result = connection.request("select max(idQ) from QUESTIONNAIRE");
		try{
			result.next();
			return result.getInt(1)+1;
		}
		catch(SQLException e){
			System.out.println(e);
		}
		return -1;
	}
	public static ArrayList<Questionnaire> getAllQuestionnaire(ConnectionMySql connection){
		ArrayList<Questionnaire> liste = new ArrayList<Questionnaire>();
		ResultSet result = connection.request("select * from QUESTIONNAIRE where Etat='C'");
		try{
			while(result.next()){
				liste.add(new Questionnaire(result.getInt("idQ"),result.getString("Titre"),result.getString("Etat"),result.getInt("idPan"), RqtConcepteur.getAllQuestion(result.getInt("idQ"), connection)));
			}
		}
		catch(SQLException e){
			return liste;
		}
		return liste;
	}

	public static void setQuestion(Question q, ConnectionMySql connection){
		Object [] elements = {q.getIdQuestionnaire(), q.getNumQuestion(), q.getEnonce(), q.getMaxVal(), Character.toString(q.getIdTypeQuestion())};
		String [] types = {"int", "int", "String", "int", "String"};
		connection.update("insert into QUESTION(idQ, numQ, texteQ, MaxVal, idT) values (?, ?, ?, ?,?)", elements, types);
	}

	public static void deleteQuestion(int idQuestionnaire, int idQuestion, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire,idQuestion};
		String [] types = {"int","int"};
		connection.update("delete from QUESTION where idQ = ? and numQ=?", elements, types);
	}

	public static void deleteQuestions(int idQuestionnaire, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire};
		String [] types = {"int"};
		RqtConcepteur.deletePropositions(idQuestionnaire,connection);
		connection.update("delete from QUESTION where idQ = ?", elements, types);
	}

	public static void updateQuestion(Question q, ConnectionMySql connection){
		RqtConcepteur.deleteQuestion(q.getIdQuestionnaire(), q.getNumQuestion(), connection);
		RqtConcepteur.setQuestion(q, connection);
	}

	public static void updateProposition(ValPossible v, ConnectionMySql connection){
		RqtConcepteur.deleteProposition(v.getIdQuestionnaire(), v.getNumQuestion(), v.getIdValPossible(), connection);
		RqtConcepteur.setProposition(v, connection);
	}

	public static void setProposition(ValPossible v, ConnectionMySql connection){
		Object [] elements = {v.getIdQuestionnaire(), v.getNumQuestion(), v.getIdValPossible(), v.getTexte()};
		String [] types = {"int", "int", "int", "String"};
		//System.out.println(connection);
		// try{
			connection.update("insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values (?, ?, ?, ?)", elements, types);
		// }
		// catch(MySQLIntegrityConstraintViolationException e){
		// 	System.out.println("Il y à déjà une réponse avec cette id");
		//  }
	}

	public static void deleteProposition(int idQuestionnaire, int idQuestion, int idReponse, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire,idQuestion, idReponse};
		String [] types = {"int","int","int"};
		connection.update("delete from VALPOSSIBLE where idQ = ? and numQ=? and idV= ?", elements, types);
	}

	public static void deletePropositions(int idQuestionnaire, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire};
		String [] types = {"int"};
		connection.update("delete from VALPOSSIBLE where idQ = ?", elements, types);
	}
	
	public static ArrayList<PanelSonde> getAllPanel(ConnectionMySql connection){
		ArrayList<PanelSonde> c = new ArrayList<PanelSonde>();
		ResultSet result = connection.request("select * from PANEL");
		try{
			while(result.next()){
				c.add(new PanelSonde(result.getInt("idPan"),result.getString("nomPan")));
			}
			return c;		
		} catch (Exception e){
			return null;
		}
	}
}