
package Modele;
public class Utilisateur {
	private int id;
	private String nom;
	private String prenom;
	private String login;
	private int idRole;
	private String role;

	public Utilisateur(int id, String nom, String prenom, String login, int idRole) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.idRole = idRole;
		this.setRole(idRole);
	}

	public Utilisateur(String nom, String prenom, String login, int idRole) {
		this.id = 0;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.idRole = idRole;
		this.setRole(idRole);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getRole() {
		return role;
	}

	private void setRole(int idRole){
		switch (idRole){
			case 1: this.role = "Concepteur"; break;
			case 2: this.role = "Sondeur"; break;
			case 3: this.role = "Analyste"; break;
		}
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getIdRole() {
		return idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}
}
