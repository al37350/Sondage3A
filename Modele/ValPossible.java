package Modele;

public class ValPossible{
	private int idQuestionnaire;
	private int numQuestion;
	private int idValPossible;
	private String texte;

	public ValPossible(int idQuestionnaire, int numQuestion, int idValPossible,String texte){
		this.texte = texte;
		this.idQuestionnaire = idQuestionnaire;
		this.numQuestion = numQuestion;
		this.idValPossible = idValPossible;
	}

	public int getIdQuestionnaire(){
		return idQuestionnaire;
	}

	public void setIdQuestionnaire(int id){
		this.idQuestionnaire = id;
	}

	public int getNumQuestion(){
		return numQuestion;
	}

	public void setNumQuestion(int num){
		this.numQuestion = num;
	}

	public String getTexte(){
		return texte;
	}

	public void setTexte(String texte){
		this.texte = texte;
	}

	public int getIdValPossible(){
		return idValPossible;
	}

	public void setIdValPossible(int nb){
		this.idValPossible = nb;
	}

	public String toString(){
		return this.texte;
	}


}