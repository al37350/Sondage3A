package Modele;

public class Sonde{
	String num;
	String nom;
	String prenom;
	String telephone;
	String titreQ; 
	String idQ;

	public Sonde(String num, String nom, String prenom, String telephone, String titreQ, String idQ){
		this.num = num;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.titreQ = titreQ;
		this.idQ = idQ;
	}

	public String getNum(){
		return this.num;
	}

	public String getNom(){
		return this.nom;
	}

	public String getPrenom(){
		return this.prenom;
	}

	public String getTelephone(){
		return this.telephone;
	}

	public String getTitreQ(){
		return this.titreQ;
	}

	public String getIdQ(){
		return this.idQ;
	}

	public String toString(){
		return this.nom+" "+this.prenom;
	}
}