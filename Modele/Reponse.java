package Modele;

import javax.swing.JCheckBox;
import java.util.ArrayList;

public class Reponse{
	char type;
	int numero;
	String texte;
	ArrayList<JCheckBox> listeRep;

	public Reponse(char type, int numero, String texte){
		this.type = type;
		this.numero = numero;
		this.texte = texte;
	}

	public Reponse(char type, int numero, ArrayList<JCheckBox> listeRep){
		this.type = type;
		this.numero = numero;
		this.listeRep = listeRep;
	}

	public int getNumero(){
		return this.numero;
	}

	public String getTexte(){
		return this.texte;
	}

	public ArrayList<JCheckBox> getListeRep(){
		return listeRep;
	}

}