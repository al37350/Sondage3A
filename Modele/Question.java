package Modele;

import java.util.ArrayList;


public class Question {
	private int idQuestionnaire;
	private int numQuestion;
	private String enonce;
	private int maxVal;
	private char idTypeQuestion;
	private ArrayList<ValPossible> lesPropositions;
	
	public Question(String s, char idTypeQuestion, int idQuestionnaire, int numQuestion){
		this.enonce=s;
		this.idTypeQuestion=idTypeQuestion;
		this.idQuestionnaire=idQuestionnaire;
		this.numQuestion=numQuestion;
		lesPropositions=new ArrayList<ValPossible>();
	}
	
	public Question(String s, int maxVal, char idTypeQuestion, int idQuestionnaire, int numQuestion){
		this(s,idTypeQuestion,idQuestionnaire,numQuestion); //appele du constructeur
		this.maxVal=maxVal;
	}

	public Question(String s, char idTypeQuestion, int idQuestionnaire, int numQuestion, ArrayList<ValPossible> liste){
		this(s,idTypeQuestion,idQuestionnaire,numQuestion);
		this.lesPropositions=liste;
	}

	public Question(String s, int maxVal, char idTypeQuestion, int idQuestionnaire, int numQuestion, ArrayList<ValPossible> liste){
		this(s,idTypeQuestion,idQuestionnaire,numQuestion);
		this.lesPropositions=liste;
		this.maxVal=maxVal;
	}


	public int getNumNextProposition(){
		if (lesPropositions.size()==0){
			return 1;
		}
		int j=lesPropositions.get(0).getIdValPossible();
		for(int i=0; i<lesPropositions.size(); i++){
			if (lesPropositions.get(i).getIdValPossible()>j){
				j=lesPropositions.get(i).getIdValPossible();
			}
		}
		return j+1;
	}

	public int getMaxVal(){
		return this.maxVal;
	}

	public void setMaxVal(int i){
		this.maxVal=i;
	}

	public char getIdTypeQuestion(){
		return idTypeQuestion;
	}

	public void setIdTypeQuestion(char c){
		this.idTypeQuestion=c;
	}

	public String getEnonce(){
		return enonce;
	}
	
	public void setEnonce(String e){
		this.enonce=e;
	}
 	
 	public int getNumQuestion(){
 		return numQuestion;
 	}

 	public void setNumQuestion(int i){
 		this.numQuestion=i;
 	}

 	public int getIdQuestionnaire(){
 		return idQuestionnaire;
 	}

 	public void setIdQuestionnaire(int i){
 		this.idQuestionnaire=i;
 	}

	public ArrayList<ValPossible> getPropositions(){
		return lesPropositions;
	}
	
	public ValPossible getProposition(int numP){
		return lesPropositions.get(numP);
	}

	public void setPropositions(ArrayList<ValPossible> reponses) {
		this.lesPropositions = reponses;
	}

	public void addProposition(ValPossible p){
		lesPropositions.add(p);
	}

	public void removeProposition(int index){
		this.lesPropositions.remove(index);
	}

	public boolean contientValPossible(ValPossible vP){
		for(ValPossible v: lesPropositions){
			if(vP.getIdQuestionnaire()==v.getIdQuestionnaire() && vP.getNumQuestion()==v.getNumQuestion() && vP.getTexte()==v.getTexte()){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString(){
		return this.getEnonce();
	}
	
}
