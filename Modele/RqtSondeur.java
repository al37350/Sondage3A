package Modele;
import base.*;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import java.sql.ResultSet;
import java.sql.*;

public class RqtSondeur extends RqtCommunes{
	private static int numeroprecedentSonde=1;

	public static ArrayList<Questionnaire> getAllQuestionnaire(ConnectionMySql connection){
		ArrayList<Questionnaire> liste = new ArrayList<Questionnaire>();
		ResultSet result = connection.request("select * from QUESTIONNAIRE where Etat='S'");
		try{
			while(result.next()){
				liste.add(new Questionnaire(result.getInt("idQ"),result.getString("Titre"),result.getString("Etat"),result.getInt("idPan"), RqtCommunes.getAllQuestion(result.getInt("idQ"), connection)));
			}
		}
		catch(SQLException e){
			return liste;
		}
		return liste;
	}

	public static Sonde getprochainSonde(ConnectionMySql connection){
		
		String [] infoSonde =  new String [6];
		ResultSet result = connection.request("select s.numSond, s.nomSond, s.prenomSond, s.telephoneSond, q.Titre, q.idQ from SONDE s,PANEL p, QUESTIONNAIRE q, CONSTITUER c where q.idPan=p.idPan and p.idPan=c.idPan and s.numSond=c.numSond and s.numSond not in (select i.numSond from INTERROGER i where i.idQ=q.idQ) and q.Etat='S' and s.numSond>="+numeroprecedentSonde+"  order by q.idQ limit 1");		
		ResultSet result2 = connection.request("select max(s.numSond) from SONDE s,PANEL p, QUESTIONNAIRE q, CONSTITUER c where q.idPan=p.idPan and p.idPan=c.idPan and s.numSond=c.numSond and q.Etat='S' and s.numSond not in (select i.numSond from INTERROGER i where i.idQ=q.idQ)");


		//mise à jour du prochain sondé
		try{

			//on est a la fin des sondés ont remet au début
			result2.next();
			if(numeroprecedentSonde>=result2.getInt(1)-1){
				numeroprecedentSonde=1;
			}
			else{

				//prochain sondé le plus proche
				ResultSet result3 = connection.request("select s.numSond from SONDE s,PANEL p, QUESTIONNAIRE q, CONSTITUER c where q.idPan=p.idPan and p.idPan=c.idPan and s.numSond=c.numSond and q.Etat='S' and s.numSond not in (select i.numSond from INTERROGER i where i.idQ=q.idQ) and s.numSond>"+numeroprecedentSonde+" order by s.numSond limit 1;");
				try{
					result3.next();
					numeroprecedentSonde=result3.getInt(1);
				}
				catch(Exception e){
					System.out.println(e);
				}		
			
			}
		}	
		catch(Exception e){
			System.out.println(e);
		}

		Sonde pS = null;

		try{
			result.next();
			infoSonde[0] = Integer.toString(result.getInt(1));
			infoSonde[1] = result.getString(2);
			infoSonde[2] = result.getString(3);

			String temp = "";
			for (int i = 0; i < result.getString(4).length(); i++){
				if (i%2 == 0 && i != 0){
					temp += " ";
				}
				temp += result.getString(4).charAt(i);
			}
			
			infoSonde[3] = temp;
			infoSonde[4] = result.getString(5);
			infoSonde[5] = Integer.toString(result.getInt(6));
			pS = new Sonde(infoSonde[0], infoSonde[1], infoSonde[2], infoSonde[3], infoSonde[4], infoSonde[5]);
			return pS;	
		} catch (Exception e){
			return pS;
		}
	}

	public static void updateQuestion(Question q, ConnectionMySql connection){
		RqtSondeur.deleteQuestion(q.getIdQuestionnaire(), q.getNumQuestion(), connection);
		RqtSondeur.setQuestion(q, connection);
	}

	public static void setQuestion(Question q, ConnectionMySql connection){
		Object [] elements = {q.getIdQuestionnaire(), q.getNumQuestion(), q.getEnonce(), q.getMaxVal(), Character.toString(q.getIdTypeQuestion())};
		String [] types = {"int", "int", "String", "int", "String"};
		connection.update("insert into QUESTION(idQ, numQ, texteQ, MaxVal, idT) values (?, ?, ?, ?,?)", elements, types);
	}

	public static void deleteQuestion(int idQuestionnaire, int idQuestion, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire,idQuestion};
		String [] types = {"int","int"};
		connection.update("delete from QUESTION where idQ = ? and numQ=?", elements, types);
	}

	public static void updateProposition(ValPossible v, ConnectionMySql connection){
		RqtSondeur.deleteProposition(v.getIdQuestionnaire(), v.getNumQuestion(), v.getIdValPossible(), connection);
		RqtSondeur.setProposition(v,connection);
	}

	public static void setProposition(ValPossible v, ConnectionMySql connection){
		Object [] elements = {v.getIdQuestionnaire(), v.getNumQuestion(), v.getIdValPossible(), v.getTexte()};
		String [] types = {"int", "int", "int", "String"};
		//System.out.println(connection);
		// try{
			connection.update("insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values (?, ?, ?, ?)", elements, types);
		// }
		// catch(MySQLIntegrityConstraintViolationException e){
		// 	System.out.println("Il y à déjà une réponse avec cette id");
		//  }
	}

	public static void deleteProposition(int idQuestionnaire, int idQuestion, int idReponse, ConnectionMySql connection){
		Object [] elements = {idQuestionnaire,idQuestion, idReponse};
		String [] types = {"int","int","int"};
		connection.update("delete from VALPOSSIBLE where idQ = ? and numQ=? and idV= ?", elements, types);
	}

	public static void sondeRepond(Sonde s, Questionnaire q, Utilisateur u, ConnectionMySql connection){
		Object [] elements = {u.getId(), Integer.parseInt(s.getNum()), q.getidQ()};
		String [] types = {"int", "int", "int"};
		connection.update("insert into INTERROGER(idU, numSond, idQ) values (?, ?, ?)", elements, types);
	}

	public static void setReponses(Questionnaire questionnaire, Reponse [] listeReponses, Sonde sonder, ConnectionMySql connection){
		// numero de categorie 
		//select idC from SONDE  natural join CARACTERISTIQUE natural join CATEGORIE limit 10;
		
		String idC="";
		String reponseBd="";
		ResultSet rs=connection.request("select idC from SONDE natural join CARACTERISTIQUE natural join CATEGORIE where numSond="+sonder.getNum());
		
		try{
			rs.next();
			idC=rs.getString("idC");
		}
		catch(Exception e){
			System.out.println(e);
		}

		for (int i=0; i<listeReponses.length; i++ ) {
			Question question=RqtCommunes.getQuestion(questionnaire.getidQ(), listeReponses[i].getNumero(), connection);
			switch(question.getIdTypeQuestion()){
				case ('c'):
				case ('m'):
					reponseBd="";
					//Pour chaque reponse du questionnaire
					for(JCheckBox rep: listeReponses[i].getListeRep()){
						//Pour chaque JCheckBox d'une reponse
						reponseBd+=rep.getName()+";";
						}
						//On rejourt

						Object [] elements = {questionnaire.getidQ(), listeReponses[i].getNumero(), idC, reponseBd};
						String [] types = {"int", "int", "String", "String"};
						connection.update("insert into REPONDRE(idQ, numQ, idC, valeur) values (?,?,?,?)", elements, types);
					break;

				case ('n'):
				case ('u'):
				case ('l'):
					reponseBd=listeReponses[i].getTexte();
					Object [] elements2 = {questionnaire.getidQ(), listeReponses[i].getNumero(), idC, reponseBd};
					String [] types2 = {"int", "int", "String", "String"};
						connection.update("insert into REPONDRE(idQ, numQ, idC, valeur) values (?,?,?,?)", elements2, types2);
					break;
			}
		}
		RqtSondeur.sondeRepond(sonder, questionnaire, Application.user,connection);
	}
}