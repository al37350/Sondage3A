package analyste;

import Modele.*;
import base.*;
import controleur.*;


import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;

public class Vue_analyste_resultat_sondage extends Vue_globale{
	public JTextField nomRep;
	
	public Vue_analyste_resultat_sondage(JFrame fenetre){
		super(fenetre, "annuler", "basic", "basic", "analyste");


		//Création des polices
		Font font1 = new Font("Calibri", Font.BOLD, 40);
		Font font2 = new Font("Calibri", 0, 40);
		Font font3 = new Font("Calibri", 0, 20);
		Font font4 = new Font("Calibri", 0, 30);


		// Création des panel et des layout
		JPanel main = new JPanel();
		main.setLayout(new GridLayout(4,1));

		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.CENTER);
		layout.setVgap(30);

		JPanel center = new JPanel();
		center.setLayout(new FlowLayout());
		center.setPreferredSize(new Dimension(1000, 800));
		
		JPanel titre = new JPanel();
		titre.setLayout(layout);

		JPanel nomFichier = new JPanel();
		titre.setLayout(layout);
		
		JPanel rep = new JPanel();
		rep.setLayout(layout);

		//Information en haut à droite
		GridLayout layoutDroite = new GridLayout(2, 1);
		layoutDroite.setVgap(-30);
		droite.setLayout(layoutDroite);
		JLabel np = new JLabel("     " + Application.user.getPrenom() + " " + Application.user.getNom());
		np.setFont(font3);
		JLabel role = new JLabel("     " + Application.user.getRole());
		role.setFont(font3);

		droite.add(np);
		droite.add(role);

		// Définition des polices
		Font fontTitre = new Font("Liberation Sans", Font.BOLD, 38);
		Font fontLabel = new Font("Calibri",0,18);
		Font fontTextField = new Font("Calibri",0,25);

		// Création du titre
		JLabel labelTitre = new JLabel("Géneration du fichier pdf");
		labelTitre.setFont(fontTitre);

		// Création de la partie nommage du fichier
		JLabel labelNomSond = new JLabel("Nom du fichier :");
		labelNomSond.setFont(fontLabel);
		JTextField nomSondage = new JTextField(20);
		nomSondage.setFont(fontTextField);
		nomSondage.setText("le_nom_du_fichier");
		JLabel labelExtension = new JLabel(".pdf");
		labelExtension.setFont(fontLabel);

		// Création du repertoire du fichier
		JLabel labelNomRep = new JLabel("Répertoire :");
		labelNomRep.setFont(fontLabel);
		nomRep = new JTextField(15);
		//nomRep.setText("/home");
		nomRep.setText(System.getProperty("user.home"));
		nomRep.setEditable(false);
		nomRep.setFont(fontTextField);

		//Creation bouton parcourir
		JButton boutonParcourir= new JButton("Parcourir...");
		boutonParcourir.addActionListener(new ActionBoutonTest(fenetre, "parcourir"));
		boutonParcourir.setPreferredSize(new Dimension(150, 30));
		boutonParcourir.setFocusPainted(false);

		//Creation JButtons:
		JPanel boutons = new JPanel();
		FlowLayout layoutBoutons = new FlowLayout();
		layoutBoutons.setHgap(80);
		layoutBoutons.setVgap(20);
		boutons.setLayout(layoutBoutons);
		

		JButton npGenerer = new JButton("Ne pas génerer");
		npGenerer.setBackground(new Color(210, 59, 59, 255));
		npGenerer.setPreferredSize(new Dimension(300, 100));
		npGenerer.setFocusPainted(false);

		JButton generer = new JButton("Génerer");
		generer.setBackground(new Color(158, 244, 109, 255));
		generer.setPreferredSize(new Dimension(300, 100));
		generer.setFocusPainted(false);
		generer.addActionListener(new ActionBoutonTest(fenetre, "generer"));

		boutons.add(npGenerer);
		boutons.add(generer);

		// Ajouts :
		//Ajout titre 
		titre.add(labelTitre);

		//Ajouts partie nommage
		nomFichier.add(labelNomSond);
		nomFichier.add(nomSondage);
		nomFichier.add(labelExtension);

		//Ajouts partie repertoire
		rep.add(labelNomRep);
		rep.add(nomRep);
		rep.add(boutonParcourir);

		// Ajouts panel principal
		main.add(titre);	
		main.add(nomFichier);	
		main.add(rep);
		main.add(boutons);

		center.add(main, "center");
		this.add(center,"Center");
	}
}

