package analyste;

import Modele.*;
import base.*;
import controleur.*;


import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;

public class Vue_analyste_question extends Vue_globale{

	private int progression, max;
	private JProgressBar progressBar;
	private JLabel legendeProgression;
	public  JPanel fond4;
	public static Questionnaire q;
	public static int numQuestion = 0;

	public Vue_analyste_question(JFrame fenetre){
		super(fenetre, "annuler", "basic", "basic", "analyste");
		this.q = Vue_analyste_accueil.q;
		AnalyseBD anal = new AnalyseBD(Application.connection);


		// font pour le titre
		Font t = new Font("Calibri", Font.PLAIN, 36);

		// fond pour le reste
		Font r = new Font("Calibri", Font.PLAIN, 20);

		// JLabel titre
		JLabel titre = new JLabel(parseString(q.getQuestion(this.numQuestion).getEnonce()));
		titre.setFont(t);

		// JLabel choix du diag
		JLabel diag = new JLabel("Associer Diagramme :");
		diag.setFont(r);

		// JLabel critere
		JLabel crit = new JLabel("Modifier critère :");
		crit.setFont(r);

		// JLabel commentaire
		JLabel com = new JLabel("Commentaire");

		// Menu pour choisir le type de diag
		String[] tab = {"1 - Aucun", "2 - Circulaire","3 - Baton","4 - Ligne"};
		JComboBox<String> choix_diag = new JComboBox<String>(tab);

		// Zone de texte pour le commentaire
		JTextField text_com = new JTextField(50);
		text_com.setPreferredSize(new Dimension(350,50));

		// JButton radio pour choix de la catégorie
		JCheckBox age = new JCheckBox("Age");
		age.setFont(r);
		JCheckBox cat = new JCheckBox("Catégorie socio-professionel");
		cat.setSelected(true);
		cat.setFont(r);

		// Regroupe les button dans un Jpanel
		JPanel choix_type = new JPanel();
		choix_type.setLayout(new BoxLayout(choix_type, BoxLayout.Y_AXIS));

		choix_type.add(age);
		choix_type.add(cat);

		// Changer de question
		BoutonPrecedent prec = new BoutonPrecedent(fenetre, "analyste");
		prec.setPreferredSize(new Dimension(125, 0));
		prec.addActionListener(new ActionBouton_analyste(fenetre,"precedent"));
		BoutonSuivant suiv = new BoutonSuivant(fenetre, "analyste");
		suiv.addActionListener(new ActionBouton_analyste(fenetre,"suivant"));
		suiv.setPreferredSize(new Dimension(125, 0));
		// Jpanel pour placement en attendant d'avoir le tableau avec les requetes sql
		JPanel remplacement = new JPanel();
		remplacement.setPreferredSize(new Dimension(550,250));
		remplacement.setBorder(new LineBorder(Color.BLACK,1));


		// Création du JTable
		JTable tableau1 = new JTable();
		JTable tableau2 = new JTable();




		// JPanel principal
		JPanel fond = new JPanel();
		fond.setLayout(new BorderLayout());
		fond.add(prec,"West");
		fond.add(suiv,"East");


		// JPanel secondaire
		JPanel fond2 = new JPanel();
		fond2.setLayout(new GridLayout(2,1));

		JPanel fond3 = new JPanel();
		fond3.setLayout(new BorderLayout());

		fond4 = new JPanel();
		fond4.setLayout(new BorderLayout());

		JPanel pos_titre = new JPanel();
		pos_titre.add(titre);

		// JPanel associer diag et son label
		JPanel label_diag = new JPanel();
		label_diag.add(diag);
		label_diag.add(choix_diag);

		// JPanel associer catg et son label
		JPanel label_catg = new JPanel();
		label_catg.add(crit);
		label_catg.add(choix_type);

		// JPanel regroupe diag et catg
		JPanel diag_catg = new JPanel();
		diag_catg.add(label_diag);
		diag_catg.add(label_catg);

		//JPanel pour com
		JPanel panel_label_com = new JPanel();
		panel_label_com.add(com);

		JPanel txt_com = new JPanel();
		txt_com.add(text_com);

		JPanel label_com = new JPanel();
		label_com.add(panel_label_com);
		label_com.add(txt_com);

		// Ajout de la bar de progression
		max = 20;
		progression = 1;
		progressBar = new JProgressBar(0, max);
		progressBar.setPreferredSize(new Dimension(300, 30));
		progressBar.setValue(1);
		progressBar.setStringPainted(false);

		// Regroupe les JPanel
		fond2.add(pos_titre);
		fond2.add(diag_catg);
		fond3.add(remplacement,"Center");
		

		fond4.add(fond2,"North");
		fond4.add(anal.analyseCat(q.getQuestion(this.numQuestion).getNumQuestion(),q.getidQ()),"Center");
		fond4.add(label_com,"South");

		fond.add(fond4);

		this.add(fond);

	}

	public String parseString(String init){
		String [] listeMot = init.split(" ");
		int taille = 0;
		String res = "<html>";
		boolean done = false;
		for (String mot: listeMot){
			if (taille < 30 | (taille > 30 && done) | mot.equals("?")){
				res +=  mot + " ";
				taille += mot.length();
			} else {
				res += "<br>" + mot + " ";
				done = true;
			}
		}
		res += "</html>";
		return res;
	}


	public void loadNext(){
		if (progression < max){
			progression += 1;
			legendeProgression.setText("Question : " + progression + "/"  + max);
			progressBar.setValue(progressBar.getValue() + 1);
		}
		this.fenetre.refresh();
	}

	public void loadPrevious(){
		if (progressBar.getValue() != 0){
			progression -= 1;
			legendeProgression.setText("Question : " + progression + "/"  + max);
			progressBar.setValue(progressBar.getValue() - 1);
		}
		this.fenetre.refresh();
	}
}