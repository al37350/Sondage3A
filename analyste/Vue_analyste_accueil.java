package analyste;

import Modele.*;
import base.*;
import controleur.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.ArrayList;


public class Vue_analyste_accueil extends Vue_globale {
	private ArrayList<Questionnaire> listeSond = new ArrayList<Questionnaire>();
	public JButton selected;
	public JLabel error;
	public static Questionnaire q;

	public Vue_analyste_accueil(JFrame fenetre){
		super(fenetre, "analyste");
		listeSond = RqtAnalyste.getAllQuestionnaire(Application.connection);
		
		// Création des panel et des layout
		JPanel princ = new JPanel();
		princ.setLayout(new FlowLayout());
		princ.setBackground(new Color(240, 235, 225, 100));


		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.CENTER);
		layout.setVgap(40);

		FlowLayout layout2 = new FlowLayout();
		layout2.setAlignment(FlowLayout.CENTER);
		layout2.setHgap(300);
		layout2.setVgap(20);
		

		JPanel titre = new JPanel();
		titre.setLayout(layout);
		titre.setBackground(new Color(240, 235, 225, 100));

		JPanel quest = new JPanel();
		quest.setLayout(layout2);
		quest.setBackground(new Color(240, 235, 225, 100));

		// Définition des polices
		Font fontTitre = new Font("Liberation Sans", Font.BOLD, 38);
		Font fontLabel = new Font("Calibri",0,18);
		Font fontTextField = new Font("Calibri",0,25);
		Font fontErreur = new Font("Calibri", Font.BOLD, 28);

		// Création du titre
		JLabel labelTitre = new JLabel("Sélectionner un sondage à analyser");
		labelTitre.setFont(fontTitre);
		titre.add(labelTitre);

		// Création du label questionnaire
		JLabel labelQuest = new JLabel("Sondages :");
		labelQuest.setFont(fontLabel);
		quest.add(labelQuest);

		// Création du panel de choix d'un sondage à analyser
		FlowLayout layout3 = new FlowLayout();
		layout3.setAlignment(FlowLayout.CENTER);
		layout3.setVgap(25); 

		JPanel choix = new JPanel();
		choix.setLayout(layout3);			
		choix.setBorder(new LineBorder(Color.black,2));				
		choix.setPreferredSize(new Dimension(1000,65*(listeSond.size()+1)));
		choix.setBackground(new Color(240, 235, 225, 100));

						
		ActionChoix ac = new ActionChoix(fenetre,this);
		int cpt = 0;

		for(int i=0;i<listeSond.size();i++){
				JButton bouton = new JButton(listeSond.get(i).getTitre());
				bouton.setFont(fontLabel);
				bouton.setName(Integer.toString(i));		
				bouton.addActionListener(ac);
				bouton.setBackground(Color.WHITE);		
				bouton.setBorder(new LineBorder(Color.black,1));
				bouton.setPreferredSize(new Dimension(900,40));
				choix.add(bouton);
				cpt+=1;
		}

		choix.setPreferredSize(new Dimension(1000,65*(cpt+1)));

		
		JButton boutonAnalyse = new JButton("Analyser");
		boutonAnalyse.setFont(fontLabel);
		boutonAnalyse.setName("analyser");
		boutonAnalyse.setBackground(Color.WHITE);
		boutonAnalyse.setBorder(new LineBorder(Color.black,2));
		boutonAnalyse.setPreferredSize(new Dimension(200,50));


		boutonAnalyse.addActionListener(ac);

		
		JScrollPane jsp = new JScrollPane();
		jsp.setViewportView(choix);		
		jsp.setPreferredSize(new Dimension(1020,300));
		jsp.getVerticalScrollBar().setUnitIncrement(10);
		jsp.setBackground(new Color(240, 235, 225, 100));
		jsp.getVerticalScrollBar().addAdjustmentListener(new ActionScroll(fenetre));


		JPanel choixQuest = new JPanel();
		choixQuest.setLayout(new BorderLayout());
		choixQuest.add(labelQuest,"North");
		choixQuest.add(jsp,"Center");
		choixQuest.setBackground(new Color(240, 235, 225, 100));

		FlowLayout layout4 = new FlowLayout(); 		
		layout4.setVgap(35); 
		layout4.setAlignment(FlowLayout.RIGHT);

		JPanel bAnalyse = new JPanel();
		bAnalyse.setPreferredSize(new Dimension(750,100));
		bAnalyse.setLayout(layout4);
		bAnalyse.setBackground(new Color(240, 235, 225, 100));

		error = new JLabel();
		error.setFont(fontErreur);
		error.setForeground(Color.RED);


		bAnalyse.add(boutonAnalyse);
		

		princ.add(titre);	
		princ.add(choixQuest);
		princ.add(bAnalyse);
		princ.add(error);
		
		selected=null;

		this.add(princ,"Center");
	}

	public JButton getSelected(){
		return this.selected;
	}

	public void setSelected(JButton s){
		this.selected=s;
	}

	public Questionnaire getQuestionnaire(int indice){
		return listeSond.get(indice);
	}
	
}
